import pika
import threading
import logging


class RabbitMQ:
    def __init__(self, host, queue, port=5672, heartbeat=60, verbosity="INFO"):
        # heartbeat of value 0 means no heartbeat. value is in seconds
        self.logger = get_create_log("RabbitMQ", verbosity)
        self.host = host
        self.port = port
        self.queue = queue
        self.heartbeat = heartbeat
        self.connection = None
        self.channel = None
        self.init_connection()

    def init_connection(self):
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=self.host, port=self.port, heartbeat=self.heartbeat))
        channel = connection.channel()
        channel.queue_declare(queue=self.queue)
        self.connection = connection
        self.channel = channel

    def publish(self, body, exchange='', routing_key=''):
        self.channel.basic_publish(exchange=exchange, routing_key=routing_key, body=body)

    def close(self):
        self.connection.close()

    def consume(self, callback=None):
        def cb(ch, method, properties, body):
            if callback is not None:
                t = threading.Thread(target=callback, args=(body,self.logger))
                t.daemon = True
                t.start()
                while t.is_alive():
                    self.logger.info("Heart beating")
                    self.connection.process_data_events()
                    self.connection.sleep(20)
            ch.basic_ack(delivery_tag=method.delivery_tag)
            self.logger.info("Finished and ack message ")
        self.channel.basic_consume(self.queue, cb)
        self.channel.start_consuming()


def get_create_log(name, level=None):
    if isinstance(level, str):
        level = getattr(logging, level)
    logger = logging.getLogger(name)
    if logging.StreamHandler in [type(handler) for handler in logger.handlers]:
        return logger
    logger.propagate = False
    console = logging.StreamHandler()
    if level is not None:
        console.setLevel(level)
        if level < logger.getEffectiveLevel():
            logger.setLevel(level)
    else:
        console.setLevel("INFO")
    formatter = logging.Formatter(f"%(asctime)s - %(name)s - %(process)d - %(levelname)s - %(message)s")
    console.setFormatter(formatter)
    logger.addHandler(console)
    logger.info("start logging with level %d", logger.getEffectiveLevel())
    return logger
