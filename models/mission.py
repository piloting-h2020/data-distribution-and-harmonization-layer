from numpy.lib import math


class Mission:
    def __init__(self, uuid=None, name=None, executionDatetime=None,
                 description=None, properties=None, inspectionPlan=None):
        if uuid is not None:
            self.UniqueID = uuid
        if name is not None:
            self.Name = name
        self.ExecutionDatetime = executionDatetime
        if description is not None and description != '':
            self.Description = description
        self.Properties = properties
        if inspectionPlan is not None:
            self.InspectionPlan = inspectionPlan

        @property
        def UUID(self):
            print("getter method called")
            return self.uuid

        @UUID.setter
        def uuid(self, uuid):
            self.uuid = uuid


class Properties:
    def __init__(self, syncId=None, imageMetadata=None, sensors=None, completed=None, mapFile=None, UTMeasurement=None,
                 scannerMetadata=None):
        if syncId is not None:
            self.SyncId = syncId
        if imageMetadata is not None:
            self.ImageMetadata = imageMetadata
        if sensors is not None:
            self.Sensors = sensors
        if completed is not None:
            self.Completed = completed
        if mapFile is not None:
            self.Map = mapFile
        if UTMeasurement is not None:
            self.UTMeasurement = UTMeasurement
        if scannerMetadata is not None:
            self.ScannerMetadata = scannerMetadata


class Sensors:
    def __init__(self, sensors):
        if sensors is not None:
            self.Sensors = sensors


class Sensor:
    def __init__(self, uuid, name, description, tf):
        self.uuid = uuid
        if name is not None:
            self.Name = name
        if description is not None and description != '':
            self.Description = description
        if tf is not None:
            self.tf = tf


class tf:
    def __init__(self, translation, rotation):
        self.Translation = translation
        self.Rotation = rotation


class Translation:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z


class Rotation:
    def __init__(self, x, y, z, w):
        self.x = x
        self.y = y
        self.z = z
        self.w = w


class ImageMetadata:
    def __init__(self, timestamp, p_x, p_y, p_z, q_x, q_y, q_z, q_w, distance, mm_per_pixel,
                 forward_vector_x, forward_vector_y, forward_vector_z,
                 left_vector_x, left_vector_y, left_vector_z, euler_angle_z_psi, euler_angle_x_theta, euler_angle_z_phi,
                 hit_point_x, hit_point_y, hit_point_z, hit_angle, spot_light, flood_light, temperature_celsius,
                 pressure_bar, pan, tilt, zoom, obj_ref_id):
        if timestamp is not None:
            self.Timestamp = timestamp
        if p_x is not None and not math.isnan(p_x):
            self.p_x = p_x
        if p_y is not None and not math.isnan(p_x):
            self.p_y = p_y
        if p_z is not None and not math.isnan(p_x):
            self.p_z = p_z
        if q_x is not None and not math.isnan(p_x):
            self.q_x = q_x
        if q_y is not None and not math.isnan(p_x):
            self.q_y = q_y
        if q_z is not None and not math.isnan(p_x):
            self.q_z = q_z
        if q_w is not None and not math.isnan(p_x):
            self.q_w = q_w
        if mm_per_pixel is not None and not math.isnan(p_x):
            self.mm_per_pixel = mm_per_pixel
        if distance is not None and not math.isnan(p_x):
            self.Distance = distance
        if forward_vector_x is not None and not math.isnan(p_x):
            self.forward_vector_x = forward_vector_x
        if forward_vector_y is not None and not math.isnan(p_x):
            self.forward_vector_y = forward_vector_y
        if forward_vector_z is not None and not math.isnan(p_x):
            self.forward_vector_z = forward_vector_z
        if left_vector_x is not None and not math.isnan(p_x):
            self.left_vector_x = left_vector_x
        if left_vector_y is not None and not math.isnan(p_x):
            self.left_vector_y = left_vector_y
        if left_vector_z is not None and not math.isnan(p_x):
            self.left_vector_z = left_vector_z
        if euler_angle_z_psi is not None and not math.isnan(p_x):
            self.euler_angle_z_psi = euler_angle_z_psi
        if euler_angle_x_theta is not None and not math.isnan(p_x):
            self.euler_angle_x_theta = euler_angle_x_theta
        if euler_angle_z_phi is not None and not math.isnan(p_x):
            self.euler_angle_z_phi = euler_angle_z_phi
        if hit_point_x is not None and not math.isnan(p_x):
            self.hit_point_x = hit_point_x
        if hit_point_y is not None and not math.isnan(p_x):
            self.hit_point_y = hit_point_y
        if hit_point_z is not None and not math.isnan(p_x):
            self.hit_point_z = hit_point_z
        if hit_angle is not None and not math.isnan(p_x):
            self.hit_angle = hit_angle
        if spot_light is not None and not math.isnan(p_x):
            self.spot_light = spot_light
        if flood_light is not None and not math.isnan(p_x):
            self.flood_light = flood_light
        if temperature_celsius is not None and not math.isnan(p_x):
            self.temperature_celsius = temperature_celsius
        if pressure_bar is not None and not math.isnan(p_x):
            self.pressure_bar = pressure_bar
        if pan is not None and not math.isnan(p_x):
            self.pan = pan
        if tilt is not None and not math.isnan(p_x):
            self.tilt = tilt
        if zoom is not None and not math.isnan(p_x):
            self.zoom = zoom
        if obj_ref_id is not None:
            self.obj_ref_id = obj_ref_id


class Valve:
    def __init__(self, basicInfo, opening_angle):
        self.BasicInfo = basicInfo
        self.Opening_angle = opening_angle


class Gauge:
    def __init__(self, basicInfo, additionalInfo):
        self.BasicInfo = basicInfo


class BasicInfo:
    def __init__(self, p_x, p_y, p_z, q_x, q_y, q_z, q_w):
        if p_x is not None:
            self.p_x = p_x
        if p_y is not None:
            self.p_y = p_y
        if p_z is not None:
            self.p_z = p_z
        if q_x is not None:
            self.q_x = q_x
        if q_y is not None:
            self.q_y = q_y
        if q_z is not None:
            self.q_z = q_z
        if q_w is not None:
            self.q_w = q_w


class ScannerMetadata:
    def __init__(self, timestamp, p_x, p_y, p_z, frame):
        if timestamp is not None:
            self.Timestamp = timestamp
        if p_x is not None and not math.isnan(p_x):
            self.p_x = p_x
        if p_y is not None and not math.isnan(p_x):
            self.p_y = p_y
        if p_z is not None and not math.isnan(p_x):
            self.p_z = p_z
        if frame is not None:
            self.Frame = frame


class ImageSize:
    def __init__(self, width, height):
        if width is not None:
            self.Width = width
        if height is not None:
            self.Height = height


class ImageFov:
    def __init__(self, horizontal, vertical):
        if horizontal is not None:
            self.Horizontal = horizontal
        if vertical is not None:
            self.Vertical = vertical


class MissionTask:
    def __init__(self, uuid=None, name=None, executionDatetime=None, notes=None, description=None, properties=None,
                 inspectionTaskId=None, inspectionTypeId=None, missionId=None):
        if uuid is not None:
            self.UUID = uuid
        self.Name = name
        self.ExecutionDatetime = executionDatetime
        if notes is not None and notes != '':
            self.Notes = notes
        if description is not None and description != '':
            self.Description = description
        if properties is not None:
            self.Properties = properties
        self.InspectionTask = inspectionTaskId
        self.InspectionType = inspectionTypeId
        self.Mission = missionId


class UTMeasurement:
    def __init__(self, timestamp=None, p_x=None, p_y=None, p_z=None, q_x=None, q_y=None,
                 q_z=None, q_w=None, p_x_final=None, p_y_final=None, p_z_final=None, forward_vector_x=None, forward_vector_y=None, forward_vector_z=None,
                 left_vector_x=None, left_vector_y=None, left_vector_z=None, euler_angle_z_psi=None,
                 euler_angle_x_theta=None, euler_angle_z_phi=None):
        self.timestamp = timestamp
        self.p_x = p_x
        self.p_y = p_y
        self.p_z = p_z
        self.q_x = q_x
        self.q_y = q_y
        self.q_z = q_z
        self.q_w = q_w
        self.p_x_final = p_x_final
        self.p_y_final = p_y_final
        self.p_z_final = p_z_final
        self.forward_vector_x = forward_vector_x
        self.forward_vector_y = forward_vector_y
        self.forward_vector_z = forward_vector_z
        self.left_vector_x = left_vector_x
        self.left_vector_y = left_vector_y
        self.left_vector_z = left_vector_z
        self.euler_angle_z_psi = euler_angle_z_psi
        self.euler_angle_x_theta = euler_angle_x_theta
        self.euler_angle_z_phi = euler_angle_z_phi


class FileMetadata:
    def __init__(self, uuid=None, url=None, mimeType=None, size=None, originalName=None, content=None,
                 creationDatetime=None, updateDatetime=None, properties=None, object_id=None, content_type=None):
        if uuid is not None:
            self.UUID = uuid
        self.URL = url
        self.MimeType = mimeType
        if size is not None:
            self.Size = size
        self.OriginalName = originalName
        self.Content = content
        if creationDatetime is not None:
            self.CreationDatetime = creationDatetime
        if updateDatetime is not None:
            self.UpdateDatetime = updateDatetime
        if properties is not None:
            self.Properties = properties
        self.object_id = object_id
        self.content_type = content_type


def my_serializer(obj):
    if type(obj) in (Mission, MissionTask, FileMetadata, Properties, ImageMetadata,
                     Sensors, Sensor, tf, Translation, Rotation, ImageSize, ImageFov,
                     Valve, Gauge, BasicInfo, UTMeasurement, ScannerMetadata):
        return vars(obj)
    else:
        return obj
