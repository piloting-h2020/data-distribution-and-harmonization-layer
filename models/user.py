class UserToken:
    def __init__(self, access_token, expires_in, refresh_expires_in, refresh_token,
                 token_type, id_token, session_state, scope,
                 expirationDate, username, password):
        self.access_token = access_token
        self.expires_in = expires_in
        self.refresh_expires_in = refresh_expires_in
        self.refresh_token = refresh_token
        self.token_type = token_type
        self.id_token = id_token
        self.session_state = session_state
        self.scope = scope
        self.expirationDate = expirationDate
        self.username = username
        self.password = password

    def update(self, newdata):
        for key, value in newdata.items():
            setattr(self, key, value)


def my_serializer(obj):
    if type(obj) in UserToken:
        return vars(obj)
    else:
        return obj