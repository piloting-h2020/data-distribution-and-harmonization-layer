class InspectionPlan:
    def __init__(self, uuid=None, name=None, description=None, asset=None):
        self.UUID = uuid
        self.Name = name
        if description is not None:
            self.Description = description
        self.Asset = asset


class Asset:
    def __init__(self, UUID=None, name=None):
        self.UUID = UUID
        if name is not None:
            self.Name = name


def InspectionPlanJsoToDto(jso, asset):
    dto = InspectionPlan(uuid=jso['UniqueID'], name=jso['Name'], description=jso['Description'], asset=asset)
    return dto


def my_serializer(obj):
    if type(obj) in (InspectionPlan, Asset, InspectionPlanJsoToDto):
        return vars(obj)
    else:
        return obj

