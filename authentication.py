import base64
import json
import os
import datetime
import requests
from datetime import timedelta

from django.contrib.auth import get_user_model
from models.user import *

from rest_framework import exceptions
from rest_framework.authentication import BaseAuthentication
from rest_framework.permissions import BasePermission


class IsAuthenticated(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        return bool(request.user and request.user.is_authenticated)


def get_authorization_header(request):
    auth = request.META.get("HTTP_AUTHORIZATION", "")
    return auth


def authenticate_user(user=None, **credentials):
    payload = {'grant_type': 'password',
               'scope': 'openid',
               'client_id': os.getenv("CLIENT_ID"),
               'client_secret': os.getenv("CLIENT_SECRET"),
               'username': credentials['username'],
               'password': credentials['password']}
    currentDatetime = datetime.datetime.utcnow()
    r = requests.post(url=os.getenv("TOKEN_URL"), data=payload)
    if r.status_code != 200:
        raise exceptions.AuthenticationFailed("Not authorized user.")
    data = r.json()
    data['expirationDate'] = str(currentDatetime + timedelta(seconds=data['expires_in']))
    data['username'] = credentials['username']
    data['password'] = credentials['password']
    del data['not-before-policy']
    if user is not None:
        user.update(json.loads(json.dumps(data)))
    else:
        user = UserToken(**json.loads(json.dumps(data)))
    return user


def update_token(token):
    credentials = {
        get_user_model().USERNAME_FIELD: token.username,
        "password": token.password
    }
    token = authenticate_user(token, **credentials)
    return token


class BasicAuthentication(BaseAuthentication):
    def authenticate(self, request):
        auth = get_authorization_header(request).split()

        if not auth or auth[0].lower() != "basic":
            raise exceptions.AuthenticationFailed("Basic Header is required!")

        if len(auth) == 1:
            raise exceptions.AuthenticationFailed("Invalid basic header. No credentials provided.")
        if len(auth) > 2:
            raise exceptions.AuthenticationFailed("Invalid basic header. Credential string is not properly formatted")
        try:
            auth_decoded = base64.b64decode(auth[1]).decode("utf-8")
            username, password = auth_decoded.split(":")
            if username == '' or password == '':
                raise exceptions.AuthenticationFailed("Username & Paasword must be given.")
        except (UnicodeDecodeError, ValueError):
            raise exceptions.AuthenticationFailed("Invalid basic header. Credentials not correctly encoded")

        return self.authenticate_credentials(username, password, request)

    @staticmethod
    def authenticate_credentials(username, password, request=None):
        credentials = {
            get_user_model().USERNAME_FIELD: username,
            "password": password
        }
        token = authenticate_user(request=request, **credentials)
        return token, None
