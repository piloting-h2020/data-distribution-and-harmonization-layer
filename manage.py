#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys
import logging
from logging.handlers import RotatingFileHandler


log_file = './logs/logs.txt'
log_dir = os.path.dirname(os.path.abspath(log_file))
if not os.path.exists(log_dir):
    os.makedirs(log_dir)

log_format = '%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s %(filename)s %(lineno)d'
logging.basicConfig(filename=log_file,
                    filemode='a',
                    format=log_format,
                    datefmt='%H:%M:%S',
                    level=logging.INFO)
logger = logging.getLogger('ddhl')
handler = RotatingFileHandler(log_file,
                              maxBytes=10000000,
                              backupCount=5)
handler.setFormatter(logging.Formatter(log_format))
logger.addHandler(handler)


def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ddhlApi.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
