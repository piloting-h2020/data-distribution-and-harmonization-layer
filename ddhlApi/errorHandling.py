from http import HTTPStatus
from django.http import HttpResponse
import uuid


class BAD_REQUEST(HttpResponse, Exception):
    status_code = HTTPStatus.BAD_REQUEST


class OK(HttpResponse):
    status_code = HTTPStatus.OK


class INTERNAL_SERVER_ERROR(HttpResponse, Exception):
    status_code = HTTPStatus.INTERNAL_SERVER_ERROR


class FOUND(HttpResponse, Exception):
    status_code = HTTPStatus.FOUND


class NOT_FOUND(HttpResponse, Exception):
    status_code = HTTPStatus.NOT_FOUND


def checkNotFound(value, literal):
    if not value:
        raise BAD_REQUEST(str(literal) + " must be given")


def checkUUIDFormat(value, literal):
    if not is_valid_uuid(value):
        raise BAD_REQUEST(str(literal) + " must be a UUID")


def checkIntegerRange(value, literal, min, max):
    if value not in range(min, max+1):
        raise BAD_REQUEST(str(literal) + "must be between " + str(min) + " and " + str(max) + ".")


def exists(value, literal):
    if len(value) == 0:
        raise NOT_FOUND(literal + "not exist!")
    else:
        return value


def not_exist(value, literal):
    if len(value) > 0:
        raise FOUND(str(literal) + " already exists!")


def is_valid_uuid(value):
    """Check if value has the UUID format"""
    try:
        uuid.UUID(value)
        return True
    except ValueError:
        return False


def is_empty_or_null(val):
    """Check if value is an empty string or null"""
    return True if val is None or val == '' else False


def is_not_empty_or_null(val):
    """Check if value isn't an empty string or null"""
    return False if val is None or val == '' else True


def find_file_with_extension(lst, ext, literal):
    """Check if list contains file with given extension and return it, else raise BAD Request"""
    for f in lst:
        if f.endswith('.' + ext):
            return f
    raise BAD_REQUEST(str(literal))
