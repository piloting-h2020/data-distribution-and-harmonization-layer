"""ddhlApi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoopproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from ddhlApi.views import commonView, gRCSView, mRCSView

urlpatterns = [

    path('admin/', admin.site.urls),

    # Inquiry Operations
    path('apis/Inspectionplan/<str:plan_id>/', commonView.getInspectionPlan),
    path('apis/Inspectionplan/site/<int:site_id>', commonView.getInspectionPlanBySite),
    path('apis/Inspectionplan/<str:plan_id>/All/', commonView.getFullInspectionPlan),
    path('apis/InspectionLocation/', commonView.getAllInspectionLocations),
    path('apis/File/Download/<str:file_id>/', commonView.getFileByFileID),
    path('apis/Inspectionplan/InspectionLocation/<str:location_id>/', commonView.getInspectionPlanByLocationID),

    # Search Operations
    # re_path(r'^apis/Inspectionplan/$', commonView.getAllInspectionPlans),

    # Post Operations
    path('apis/Mission/gRCS/', gRCSView.gRCSView().as_view()),
    path('apis/Mission/mRCS/AEROCAM/', mRCSView.mRCSView.as_view()),
    path('apis/Mission/mRCS/TTDRONE/', mRCSView.mRCSView.as_view()),
    path('apis/Mission/mRCS/VIAD_DRONE/', mRCSView.mRCSView.as_view()),
    path('apis/Mission/mRCS/VIAD_DRONE/Installation/', mRCSView.mRCSView.as_view()),
    path('apis/Mission/mRCS/ETH/', mRCSView.mRCSView.as_view()),
    path('apis/Mission/mRCS/BIKE/', mRCSView.mRCSView.as_view()),
    path('apis/Mission/mRCS/SATELITE/', mRCSView.mRCSView.as_view()),
    path('apis/Mission/mRCS/CART/', mRCSView.mRCSView.as_view()),
    path('apis/Mission/mRCS/AEROX/', mRCSView.mRCSView.as_view()),
    path('apis/Mission/mRCS/RISING/', mRCSView.mRCSView.as_view()),

    # Patch Operations
    path('apis/Mission/AI/ImageMetadata/', commonView.updateImageMetadataWithAIPredictions),
    # path('apis/ActivateInspectionPlan/<str:plan_id>/', commonView.activateInspectionPlan),
    # path('apis/DeactivateInspectionPlan/<str:plan_id>/', commonView.deactivateInspectionPlan),

    # Delete Operations
    path('apis/Files/', commonView.deleteFiles),
]
