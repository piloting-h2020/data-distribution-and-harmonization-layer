import conversion_tool.functions
from django.http import JsonResponse, HttpResponse, FileResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView, status
import authentication
import ddhlApi.dms as dms
from ddhlApi.enum import Operations as Op
from ddhlApi.enum import *
from ddhlApi.schemas import *
from jsonschema import *
from ddhlApi.utils import *
import logging
from ddhlApi.views.helpers.commonViewsHelper import *

logger = logging.getLogger('ddhl.common')


class AuthenticatedView(APIView):
    token = authentication.IsAuthenticated

    def get(self, request):
        msg = {'message': f'Hi {request.user}! Congratulations on being authenticated!'}
        return Response(msg, status=status.HTTP_200_OK)


@api_view(['GET'])
def getInspectionPlan(request, plan_id):
    try:
        logger.info("Get Inspection Plan")
        er.checkNotFound(plan_id, 'Inspection Plan ID')
        er.checkUUIDFormat(plan_id, 'Inspection Plan ID')
        response = dms.getInspectionPlan(plan_id, Op.inspectionPlan, request.user)
        return JsonResponse(response, safe=False)
    except er.BAD_REQUEST as e:
        return JsonResponse(str(e.args[0]), status=er.BAD_REQUEST.status_code, safe=False)
    except:
        return JsonResponse("Something went wong!", status=er.INTERNAL_SERVER_ERROR.status_code, safe=False)


@api_view(['GET'])
def getInspectionPlanBySite(request, site_id):
    try:
        logger.info("Get Inspection Plan")
        er.checkIntegerRange(site_id, 'Site ID ', min=1, max=len(Sites))
        plans = dms.getAllInspectionPlans(request, request.user)
        response = filter_plans_and_group_by_site(plans, request, site_id)
        # Other Solution
        # response = dms.getInspectionPlanBySiteLiteral(Sites[Experiments(site_id).name].value, request.user)
        return JsonResponse(response, safe=False)
    except er.BAD_REQUEST as e:
        return JsonResponse(str(e.args[0]), status=er.BAD_REQUEST.status_code, safe=False)
    except:
        return JsonResponse("Something went wong!", status=er.INTERNAL_SERVER_ERROR.status_code, safe=False)


@api_view(['GET'])
def getFileByFileID(request, file_id):
    filename = ''
    try:
        logger.info("Get File by File Metadata ID")
        er.checkNotFound(file_id, 'File ID')
        file = dms.getFileByID(file_id, Op.file, request.user)
        filename = file.headers.get('content-disposition')
        open(filename, 'wb').write(file.content)
        response = FileResponse(open(filename, "rb"), as_attachment=True)
        return response
    except (er.NOT_FOUND, er.BAD_REQUEST) as e:
        return JsonResponse(str(e.args[0]), status=e.status_code, safe=False)
    except:
        return JsonResponse("Something went wong!", status=er.INTERNAL_SERVER_ERROR.status_code, safe=False)
    finally:
        os.remove(filename) if filename != '' else None


@api_view(['GET'])
def getFullInspectionPlan(request, plan_id):
    try:
        logger.info("Get Full Inspection Plan")
        er.checkNotFound(plan_id, 'Inspection Plan ID')
        er.checkUUIDFormat(plan_id, 'Inspection Plan ID')
        response = dms.getInspectionPlan(plan_id, Op.fullInspectionPlan, request.user)
        if response and response[0]['InspectionTask']:
            insTaskIDs = [element['UniqueID'] for element in response[0]['InspectionTask']]
            response[0]['InspectionTask'] = dms.getFullInspectionTasks(insTaskIDs, Op.fullInspectionTask, request.user)
        return JsonResponse(response, safe=False)
    except er.BAD_REQUEST as e:
        return JsonResponse(str(e.args[0]), status=er.BAD_REQUEST.status_code, safe=False)
    except:
        return JsonResponse("Something went wong!", status=er.INTERNAL_SERVER_ERROR.status_code, safe=False)


@api_view(['GET'])
def getAllInspectionPlans(request, **kwargs):
    logger.info("Get Inspection Plans with criteria")
    response = dms.getAllInspectionPlans(request, request.user)
    return JsonResponse(response, safe=False)


@api_view(['GET'])
def getAllInspectionLocations(request):
    logger.info("Get All Inspection Locations")
    response = dms.getAllInspectionLocations(request, request.user)
    return JsonResponse(response, safe=False)


@api_view(['GET'])
def getInspectionPlanByLocationID(request, location_id):
    logger.info("Get Inspection Plan by Inspection Location ID")
    er.checkNotFound(location_id, 'Inspection Plan ID')
    er.checkUUIDFormat(location_id, 'Inspection Plan ID')
    tasks = dms.getAllInspectionTasksByLocationID(location_id, Op.inspectionTask, request.user)
    plansPerAsset = consolidate_plans_per_asset(tasks)
    response = create_list_of_inspection_plans(plansPerAsset, request.user)
    return JsonResponse(response, safe=False)


@api_view(['PATCH'])
def updateImageMetadataWithAIPredictions(request):
    try:
        logger.info("Patch Image Metadata with Annotations")
        er.checkNotFound(request.data, "Data ")
        validate(instance=request.data, schema=predictionSchema)
        for rec in request.data:
            fileMetadata = dms.getFileMetadata(rec['fileMetadataID'], Op.fileMetadata, request.user)[0]
            # del fileMetadata['Properties']['Annotations']
            annot = {'algoType': AlgoType(rec['algoType']).name, 'regions': rec['regions']}
            annotation = conversion_tool.functions.convert_annotations(annot)
            if fileMetadata['Properties'] is None:
                fileMetadata['Properties'] = {}
            if 'Annotations' in fileMetadata['Properties']:
                fileMetadata['Properties']['Annotations'].append(annotation)
            else:
                fileMetadata['Properties']['Annotations'] = []
                fileMetadata['Properties']['Annotations'].append(annotation)
            dms.patchFileMetadata(fileMetadata, request.user)
        return HttpResponse('')

    except (er.FOUND, er.BAD_REQUEST) as error:
        return JsonResponse(str(error.args[0]), status=error.status_code, safe=False)
    except exceptions.ValidationError as error:
        return JsonResponse(error.message, status=er.BAD_REQUEST.status_code, safe=False)
    except:
        return JsonResponse("Something went wong!", status=er.INTERNAL_SERVER_ERROR.status_code, safe=False)


@api_view(['PATCH'])
def activateInspectionPlan(request, plan_id):
    try:
        logger.info("Activate Inspection Plan")
        er.checkNotFound(plan_id, 'Inspection Plan ID')
        er.checkUUIDFormat(plan_id, 'Inspection Plan ID')
        plan = dms.getInspectionPlan(plan_id, Op.inspectionPlan, request.user)[0]
        plan['Properties']['Status'] = 'active'
        dms.patchInspectionPlan(plan, request.user)
        return HttpResponse('')
    except er.BAD_REQUEST as e:
        return JsonResponse(str(e.args[0]), status=er.BAD_REQUEST.status_code, safe=False)
    except:
        return JsonResponse("Something went wong!", status=er.INTERNAL_SERVER_ERROR.status_code, safe=False)


@api_view(['PATCH'])
def deactivateInspectionPlan(request, plan_id):
    try:
        logger.info("Activate Inspection Plan")
        er.checkNotFound(plan_id, 'Inspection Plan ID')
        er.checkUUIDFormat(plan_id, 'Inspection Plan ID')
        plan = dms.getInspectionPlan(plan_id, Op.inspectionPlan, request.user)[0]
        if len(dms.getMissionsByInspectionPlan(plan_id, Op.mission, request.user)[0]['Mission']) != 0:
            er.FOUND("The InspectionPlan cannot be deactivated.")
        plan['Properties']['Status'] = 'inactive'
        dms.patchInspectionPlan(plan, request.user)
        return HttpResponse('')
    except (er.BAD_REQUEST, er.FOUND) as e:
        return JsonResponse(str(e.args[0]), status=er.BAD_REQUEST.status_code, safe=False)
    except:
        return JsonResponse("Something went wong!", status=er.INTERNAL_SERVER_ERROR.status_code, safe=False)


@api_view(['DELETE'])
def deleteFiles(request):
    try:
        logger.info("Delete Files")
        if request.data['files']:
            for file in request.data['files']:
                er.checkUUIDFormat(file, 'File UUID')
                try:
                    dms.deleteFile(file, Op.delete, request.user)
                except:
                    continue
        if request.data['filesMeta']:
            for fileMeta in request.data['filesMeta']:
                er.checkUUIDFormat(fileMeta, 'File UUID')
                try:
                    dms.deleteFileMetadata(fileMeta, Op.delete, request.user)
                except:
                    continue
        return HttpResponse('')
    except (er.BAD_REQUEST, er.FOUND) as e:
        return JsonResponse(str(e.args[0]), status=er.BAD_REQUEST.status_code, safe=False)
    except:
        return JsonResponse("Something went wong!", status=er.INTERNAL_SERVER_ERROR.status_code, safe=False)
