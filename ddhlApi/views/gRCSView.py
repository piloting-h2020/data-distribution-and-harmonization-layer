import logging

from django.http import JsonResponse
from rest_framework.views import APIView
from ddhlApi.views.helpers.gRCSVIewsHelper import *
logger = logging.getLogger('ddhl.gRCS')


class gRCSView(APIView):
    @staticmethod
    def post(request):
        return postGenericData(request)


def postGenericData(request, path=None):
    response = {"mission": '', "missionTasks": [], "files": [], "fileMetadata": []}
    try:
        logger.info("Post Generic Data")
        path, inputFiles = checkZipFIleAndUnzip(request)
        # Input: Mission
        mission_json = loadMission(inputFiles)
        # Validate input
        validateInputData(mission_json, request.user)
        # Post Mission, Mission Tasks and Files with metadata
        createMission(mission_json, response, request.user)
        createMissionTasks(mission_json, response, request.user)
        createFilesWithMetadata(inputFiles, response, request.user)
        logger.info("Success Response: " + str(response))
        return JsonResponse(response, safe=False)

    except (er.FOUND, er.BAD_REQUEST) as error:
        return JsonResponse(str(error.args[0]), status=error.status_code, safe=False)
    except exceptions.ValidationError as error:
        return JsonResponse(error.message, status=er.BAD_REQUEST.status_code, safe=False)
    except:
        deleteObjects(response, request.user)
        return JsonResponse("Something went wong!", status=er.INTERNAL_SERVER_ERROR.status_code, safe=False)
    finally:
        remove_folder(path) if path is not None else None
