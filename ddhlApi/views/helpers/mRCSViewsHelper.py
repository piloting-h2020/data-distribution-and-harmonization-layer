import logging

import PIL
from PIL import Image
from jsonschema import *
import ddhlApi.dms as dms
from ddhlApi.enum import Operations as Op
from ddhlApi.schemas import *
from ddhlApi.utils import *
from models.mission import *
from ddhlApi.enum import *
import ddhlApi.errorHandling as er
import magic
import pika
import time
from pika.exceptions import AMQPConnectionError
from rabbitMQ.rabbitMQ import RabbitMQ

logger = logging.getLogger('ddhl.mRCS.general')


def postmRCSFiles(inputFiles, inspTaskToMissionTask, mission_json, path, response, token, case):
    """ Load metadata on different dataframes and Post them to the DMS"""

    if case == Case.RISING.name:
        pictures_lst = [rec for rec in mission_json['files'] if str(rec).startswith('pictures_folder_') and not str(rec).endswith('metadata')]
        sensors = [mission_json[rec] for rec in mission_json if str(rec).endswith('pictures')]
        post_rising_pictures(inputFiles, inspTaskToMissionTask, mission_json, pictures_lst, response, sensors, token)
    if 'pictures_metadata' in mission_json['files']:
        if mission_json['files']['pictures_metadata'] in inputFiles:
            names = ['file_name', 'timestamp', 'inspection_task_uuid', 'p_x', 'p_y', 'p_z', 'q_x', 'q_y', 'q_z', 'q_w',
                     'distance', 'mm_per_pixel']
            if 'haptic_data' in mission_json['files'] or 'gauge_readings' in mission_json['files']:
                del names[-2:]
                names.append('obj_ref')
            pictures_meta_df = pd.read_csv(inputFiles[mission_json['files']['pictures_metadata']], names=names,
                                           skipinitialspace=True)
            postImages(inputFiles, inspTaskToMissionTask, pictures_meta_df, response, mission_json['pictures'], token)
        elif case == Case.CART.name:
            logger.info("CART case - images not included!")
        else:
            raise er.BAD_REQUEST('Pictures metadata must be included!')
    if 'telemetry_data' in mission_json['files']:
        telemetry_df = pd.read_csv(inputFiles[mission_json['files']['telemetry_data']], names=[
            'timestamp', 'inspection_task_uuid', 'p_x', 'p_y', 'p_z', 'q_x', 'q_y', 'q_z', 'q_w'], skipinitialspace=True)
        postFilesWithMetadata(inspTaskToMissionTask, path, response, telemetry_df, MetadataType.localization.value,
                              token)
    if 'gps_data' in mission_json['files']:
        gps_df = pd.read_csv(inputFiles[mission_json['files']['gps_data']], names=[
            'timestamp', 'inspection_task_uuid', 'latitude', 'longitude', 'altitude'])
        postFilesWithMetadata(inspTaskToMissionTask, path, response, gps_df, MetadataType.gps.value, token)
    if 'installation_log' in mission_json['files']:
        installation_log_df = pd.read_csv(inputFiles[mission_json['files']['installation_log']], names=[
            'timestamp', 'inspection_task_uuid', 'p_x', 'p_y', 'p_z', 'success'])
        postFilesWithMetadata(inspTaskToMissionTask, path, response, installation_log_df,
                              MetadataType.installation_log.value, token)
    if 'haptic_data' in mission_json['files']:
        haptic_data_df = pd.read_csv(inputFiles[mission_json['files']['haptic_data']], names=[
            'timestamp', 'inspection_task_uuid', 'obj_ref', 'angle', 'torque'])
        postFilesWithMetadata(inspTaskToMissionTask, path, response, haptic_data_df, MetadataType.haptic_data.value,
                              token)
    if 'objects' in mission_json['files']:
        objects_df = pd.read_csv(inputFiles[mission_json['files']['objects']], names=[
            'timestamp', 'inspection_task_uuid', 'obj_ref_id', 'obj_type', 'p_x', 'p_y', 'p_z', 'q_x', 'q_y', 'q_z', 'q_w',
            'rotation_axis_x', 'rotation_axis_y', 'rotation_axis_z', 'opening_angle', 'obj_info_1', 'obj_info_2'])
        postFilesWithMetadata(inspTaskToMissionTask, path, response, objects_df, MetadataType.objects_found.value,
                              token)
    if 'gauge_readings' in mission_json['files']:
        gauge_readings_df = pd.read_csv(inputFiles[mission_json['files']['gauge_readings']], names=[
            'timestamp', 'inspection_task_uuid', 'obj_ref', 'value', 'unit'])
        postFilesWithMetadata(inspTaskToMissionTask, path, response, gauge_readings_df, MetadataType.gauge_readings.value,
                              token)
    if 'laser_scanner_folder' in mission_json['files']:
        df = load_df(inputFiles,  '.pcd')
        if df is not None:
            postAttachmentsWithMetadata(inputFiles, path, inspTaskToMissionTask, df, response,
                                        MetadataType.laser_scanner_metadata.value, token, case)


def post_rising_pictures(inputFiles, inspTaskToMissionTask, mission_json, pictures_lst, response, sensors, token):
    """ Find images and post them to the DMS - RISING RCS"""
    for rec in pictures_lst:
        folder = mission_json['files'][rec]
        file = folder + '_' + mission_json['files'][str(rec) + '_metadata']
        if file in inputFiles and os.path.isfile(inputFiles[file]):
            post_rising_pictures_per_type(file, folder, inputFiles, inspTaskToMissionTask, response, sensors, token)


def post_rising_pictures_per_type(file, folder, inputFiles, inspTaskToMissionTask, response, sensors, token):
    """ POST images for RISING RCS"""
    names = ['file_name', 'timestamp', 'inspection_task_uuid', 'p_x', 'p_y', 'p_z', 'q_x', 'q_y', 'q_z',
             'q_w', 'distance', 'mm_per_pixel']
    pictures_meta_df = pd.read_csv(inputFiles[file], names=names, skipinitialspace=True)
    pictures_meta_df.file_name = folder + '_' + pictures_meta_df.file_name
    sensor_pic = ''
    for sensor in sensors:
        if sensor["folder"] == folder:
            sensor_pic = sensor
    postImages(inputFiles, inspTaskToMissionTask, pictures_meta_df, response, sensor_pic,
               token)


def postBIKEmRCSFiles(inputFiles, inspTaskToMissionTask, mission_json, path, response, token, case, missionID):
    """ Load metadata on dataframes, post Images, Telemetry files and UT Measurements for each capture of BIKE
     and SATELITE RCS as well as the stl file of the inspection pipe"""
    pictures_meta_df, telemetry_df, ut_measurements_df, attachment_df = instantiate_BIKE_df()
    for capture in mission_json['Captures']:
        timestamp = time_date_to_datetime(capture['System Time'])
        add_to_telemetry_df(capture, telemetry_df, timestamp)
        picture_meta_handler(capture, inputFiles, pictures_meta_df, timestamp)
        if capture['Capture Type'] == CaptureType.ut.value:
            UT_data_handler(capture, inputFiles, inspTaskToMissionTask, path, response, timestamp, token, ut_measurements_df)
        else:
            attachment_data_handler(capture, inputFiles, attachment_df, timestamp)
    if pictures_meta_df.values.size > 0:
        postImages(inputFiles, inspTaskToMissionTask, pictures_meta_df, response, None, token)
    if ut_measurements_df.values.size > 0:
        postFilesWithMetadata(inspTaskToMissionTask, path, response, ut_measurements_df, MetadataType.UT_measurements.value, token)
    if telemetry_df.values.size > 0:
        postFilesWithMetadata(inspTaskToMissionTask, path, response, telemetry_df, MetadataType.localization.value, token)
    if attachment_df.values.size > 0:
        postAttachmentsWithMetadata(inputFiles, path, inspTaskToMissionTask, attachment_df, response, MetadataType.attachment.value, token)
    try:
        file = er.find_file_with_extension(inputFiles.keys(), 'stl', 'Data must contain map file of the pipe')
        fileID = dms.postFile(inputFiles[file], token)
        createMapFileMetadata(inputFiles[file], fileID, response, token, missionID)
    except er.BAD_REQUEST:
        logger.error("Map File does not exist")
        if case.name == Case.SATELITE.name:
            raise er.BAD_REQUEST('Map File of the inspection pipe must be provided')


def picture_meta_handler(capture, inputFiles, pictures_meta_df, timestamp):
    """Populate pictures dataframe for BIKE and SATELITE RCS"""
    if capture['Capture Type'] == CaptureType.visual.value:
        if er.is_empty_or_null(capture[CameraType.inspection.value['name']])\
                and er.is_empty_or_null(capture[CameraType.navigation.value['name']]):
            raise er.BAD_REQUEST('Visual capture should contain Inspection or Navigation Images')
    else:
        if er.is_empty_or_null(capture[CameraType.ut.value['name']])\
                and er.is_empty_or_null(capture[CameraType.imported.value['name']])\
                and er.is_empty_or_null(capture[CameraType.screenshot.value['name']]):
            return
    capture_images = {"ai": [], "not_ai": []}
    feed_capture_images(capture, capture_images)
    for ai, images in capture_images.items():
        includeAI = True if ai == AITags.ai.value else False
        for image in images:
            if image in inputFiles:
                add_to_pictures_meta_df(capture, image, pictures_meta_df, timestamp, includeAI)


def feed_capture_images(capture, capture_images):
    """Separate images into two categories: those that will successfully
    undergo analysis by the AI algorithm, and those that will not."""
    camera_types = {rec.value['name']: rec.value['tag'] for rec in CameraType}
    ai_tags = AITags.values()
    for rec in camera_types.items():
        if er.is_not_empty_or_null(capture[rec[0]]):
            extend_or_append_capture_images(capture[rec[0]], rec[1].value, capture_images)
    for rec in ai_tags:
        if capture_images[rec] is not None:
            capture_images[rec] = ['capture_' + str(capture['Capture']) + '_' + elem for elem in capture_images[rec]]


def extend_or_append_capture_images(capture, ai_tag, capture_images):
    capture_images[ai_tag].extend(str(capture).split('\n')) if '\n' in capture else capture_images[ai_tag].append(capture)


def attachment_data_handler(capture, inputFiles, attachment_df, timestamp):
    """Populate attachments dataframe for BIKE and SATELITE RCS"""
    if er.is_empty_or_null(capture['Attachments']):
        if capture['Capture Type'] == CaptureType.attachment.value:
            raise er.BAD_REQUEST('Missing attachments.')
        else:
            return
    files = []
    files.extend(str(capture['Attachments']).split('\n')) if '\n' in capture["Attachments"] else files.append(capture['Attachments'])
    files = ['capture_' + str(capture['Capture']) + '_' + elem for elem in files]
    for f in files:
        if f not in inputFiles:
            raise er.BAD_REQUEST('File ' + f + 'does not exist!')
        add_to_attachment_meta_df(capture, f, attachment_df, timestamp)


def instantiate_BIKE_df():
    """Initialise dataframes for BIKE and SATELITE RCS cases"""
    telemetry_df = pd.DataFrame(
        columns=['timestamp', 'inspection_task_uuid', 'p_x', 'p_y', 'p_z', 'q_x', 'q_y', 'q_z', 'q_w',
                 'forward_vector_x', 'forward_vector_y', 'forward_vector_z', 'left_vector_x', 'left_vector_y',
                 'left_vector_z', 'euler_angle_z_psi', 'euler_angle_x_theta', 'euler_angle_z_phi'])
    pictures_meta_df = pd.DataFrame(
        columns=['ai', 'file_name', 'timestamp', 'inspection_task_uuid', 'p_x', 'p_y', 'p_z', 'q_x', 'q_y', 'q_z', 'q_w',
                 'forward_vector_x', 'forward_vector_y', 'forward_vector_z', 'left_vector_x', 'left_vector_y',
                 'left_vector_z', 'euler_angle_z_psi', 'euler_angle_x_theta', 'euler_angle_z_phi',
                 'hit_point_x', 'hit_point_y', 'hit_point_z', 'distance', 'hit_angle', 'spot_light', 'flood_light',
                 'horizontal_fov', 'vertical_fov', 'temperature_celsius', 'pressure_bar', 'pan', 'tilt', 'zoom'])
    ut_measurements_df = pd.DataFrame(
        columns=['timestamp', 'inspection_task_uuid', 'p_x', 'p_y', 'p_z', 'q_x', 'q_y', 'q_z', 'q_w',
                 'forward_vector_x', 'forward_vector_y', 'forward_vector_z', 'left_vector_x', 'left_vector_y',
                 'left_vector_z', 'euler_angle_z_psi', 'euler_angle_x_theta', 'euler_angle_z_phi', 'thickness_type',
                 'thickness_value_mm', 'thickness_file_ID'])
    attachments_df = pd.DataFrame(
        columns=['file_name', 'timestamp', 'inspection_task_uuid'])
    return pictures_meta_df, telemetry_df, ut_measurements_df, attachments_df


def postAEROXmRCSFiles(inputFiles, inspTaskToMissionTask, mission_json, path, response, token):
    """Load UT dataframe and post File and File metadata Objects to the DMS"""
    ut_measurements_df = pd.DataFrame(
        columns=['timestamp', 'inspection_task_uuid', 'p_x', 'p_y', 'p_z', 'p_x_final', 'p_y_final', 'p_z_final',
                 'thickness_type', 'thickness_value_mm', 'thickness_file_ID'])
    temp = {'thickness_type': 'None', 'thickness_value_mm': 'None', 'file': '', 'fileID': 'None'}
    for rec in mission_json['data']:
        timestamp = time_date_to_datetime(rec['timestamp'])
        if er.is_empty_or_null(rec["attachment"]):
            raise er.BAD_REQUEST('In case of UT capture type, UT attachment must be provided.')
        temp['thickness_type'] = 'line/area'
        files = [rec['attachment']]
        file = er.find_file_with_extension(files, 'tsv', 'Attachments must contain tsv file')
        if file not in inputFiles:
            raise er.BAD_REQUEST('File ' + file + 'does not exist!')
        temp['file'] = inputFiles[file]
        post_UT_Thickness_Files_With_Metadata(rec, inspTaskToMissionTask, path, response, temp, timestamp, token, Case.AEROX.name)
        add_to_ut_measurements_df(rec, temp['fileID'], temp['thickness_type'], temp['thickness_value_mm'],
                                  timestamp, ut_measurements_df, Case.AEROX.name)
    if ut_measurements_df.values.size > 0:
        postFilesWithMetadata(inspTaskToMissionTask, path, response, ut_measurements_df, MetadataType.UT_measurements.value, token)


def UT_data_handler(capture, inputFiles, inspTaskToMissionTask, path, response, timestamp, token, ut_measurements_df):
    """Instantiate UT Thickness Objects and Metadata and post them to the DMS if needed.
    Updates UT Measurement dataframe"""
    temp = {'thickness_type': 'None', 'thickness_value_mm': 'None', 'file': '', 'fileID': 'None'}
    if er.is_empty_or_null(capture["UT CH_A Thickness [mm]"]) and er.is_empty_or_null(capture["Attachments"]):
        raise er.BAD_REQUEST('In case of UT capture type, either UT CH A Thickness or UT attachment must be provided.')
    if er.is_not_empty_or_null(capture["UT CH_A Thickness [mm]"]) and capture["UT CH_A Thickness [mm]"] != 0:
        temp['thickness_type'] = 'point'
        temp['thickness_value_mm'] = capture['UT CH_A Thickness [mm]']
    else:
        temp['thickness_type'] = 'line/area'
        files = []
        if '\n' in capture["Attachments"]:
            files.extend(str(capture['Attachments']).split('\n'))
        else:
            files.append(capture['Attachments'])
        file = er.find_file_with_extension(files, 'tsv', 'Attachments must contain tsv file')
        if file not in inputFiles:
            tempFile = 'capture_' + str(capture['Capture']) + '_' + file
            if tempFile not in inputFiles:
                raise er.BAD_REQUEST('File ' + file + 'does not exist!')
            file = tempFile
        temp['file'] = inputFiles[file]
        post_UT_Thickness_Files_With_Metadata(capture, inspTaskToMissionTask, path, response,
                                              temp, timestamp, token, Case.BIKE.name)
    add_to_ut_measurements_df(capture, temp['fileID'], temp['thickness_type'], temp['thickness_value_mm'], timestamp,
                              ut_measurements_df, Case.BIKE.name)


def post_UT_Thickness_Files_With_Metadata(capture, inspTaskToMissionTask, path, response, temp, timestamp, token, case):
    """Instantiate Thickness File and Metadata objects and post them to the DMS"""
    missionTaskID = inspTaskToMissionTask[capture['Inspection Task UUID' if case == Case.BIKE.name else 'inspectionTaskUuid']]
    fileType = path + 'UT_thickness' + '.tsv'
    temp['fileID'] = dms.postFile(temp['file'], token)
    url = str(os.getenv("DMS_FILE_URL") + str(temp['fileID']) + '/')
    response['UT_thickness'].append({'fileMetadataID': '', 'url': url})
    if case == Case.AEROX.name:
        utMeasurement = UTMeasurement(timestamp=timestamp,
                                      p_x=capture['initialPosition']['x'],
                                      p_y=capture['initialPosition']['y'],
                                      p_z=capture['initialPosition']['z'],
                                      p_x_final=capture['finalPosition']['x'],
                                      p_y_final=capture['finalPosition']['y'],
                                      p_z_final=capture['finalPosition']['z'])
    else:
        utMeasurement = UTMeasurement(timestamp=timestamp,
                                      p_x=capture['UT Probe x [m]'],
                                      p_y=capture['UT Probe y [m]'],
                                      p_z=capture['UT Probe z [m]'],
                                      q_x=capture['UT Probe Quaternion real'],
                                      q_y=capture['UT Probe Quaternion i'],
                                      q_z=capture['UT Probe Quaternion j'],
                                      q_w=capture['UT Probe Quaternion k'],
                                      forward_vector_x=capture['UT Probe Forward Vector x [m]'],
                                      forward_vector_y=capture['UT Probe Forward Vector y [m]'],
                                      forward_vector_z=capture['UT Probe Forward Vector z [m]'],
                                      left_vector_x=capture['UT Probe Left Vector x [m]'],
                                      left_vector_y=capture['UT Probe Left Vector y [m]'],
                                      left_vector_z=capture['UT Probe Left Vector z [m]'],
                                      euler_angle_z_psi=capture["UT Probe Euler Angle Z' [deg]"],
                                      euler_angle_x_theta=capture["UT Probe Euler Angle X'' [deg]"],
                                      euler_angle_z_phi=capture["UT Probe Euler Angle Z''' [deg]"])
    properties = Properties(UTMeasurement=utMeasurement)
    metaFileID = createFileMetadata(temp['file'], fileType, missionTaskID, url, token, properties)
    response['UT_thickness'][-1]['fileMetadataID'] = metaFileID


def publishRabbitMQMessage(response, rabMQ, case, inspectionPlanUuid=None, token=None, max_retries=2, retry_delay=1):
    """Publish message to RabbitMQ"""
    data = {'mission': response['mission'], 'images': response['images']}
    if case.name in [Case.CART.name, Case.TTDRONE.name]:
        assetName = dms.getInspectionPlan(inspectionPlanUuid, Op.inspectionPlan, token)[0]['Properties']['site']
        data['tunnelName'] = assetName
    for retry in range(0, max_retries):
        try:
            rabMQ.publish(json.dumps(data), routing_key=os.getenv(case.value['queue'][0]))
            rabMQ.close()
            break  # Connection successful, exit the loop
        except pika.exceptions.AMQPConnectionError:
            if retry == max_retries:
                logger.error("RabbitMQ Error Data" + str(data))
                raise er.INTERNAL_SERVER_ERROR()
            logger.error("RabbitMQ Connection Error")
            time.sleep(retry_delay)
            rabMQ = RabbitMQ(host=os.getenv("RABBITMQ_HOST"), port=os.getenv("RABBITMQ_PORT"),
                             queue=os.getenv(case.value['queue'][0]), heartbeat=int(os.getenv("RABBITMQ_HEARTBEAT")))


def deleteObjects(response, token):
    """Delete File and Metadata Objects from the DMS"""
    if response:
        for s in ['images', 'localization_telemetry', 'gps_telemetry', 'haptic_data', 'UT_measurements',
                  'UT_thickness', "objects_found", "other", "attachments", 'gauge_readings']:
            if s not in response.keys():
                continue
            for rec in response[s]:
                if rec['fileMetadataID'] != '':
                    dms.deleteFileMetadata(rec['fileMetadataID'], Op.delete, token)
                if rec['url'] != '':
                    dms.deleteFile(str(rec['url']).rsplit('/', 2)[1], Op.delete, token)


def patchMission(mission_json, retrievedMission, token):
    """Update Mission Object and save it to the DMS"""
    sensors = []
    syncId = mission_json['syncId'] if 'syncId' in mission_json.keys() else mission_json['Sync ID']
    executionDatetime = time_to_datetime(mission_json['startDate']) if 'startDate' in mission_json.keys() else time_date_to_datetime(mission_json['Inspection Start Date'])
    mapFile = mission_json['map'] if 'map' in mission_json.keys() else None
    if 'sensors' in mission_json.keys() and mission_json['sensors'] is not None:
        for rec in mission_json['sensors']:
            sensor = Sensor(uuid=rec['uuid'],
                            name=rec['name'],
                            description=rec['description'],
                            tf=tf(translation=Translation(x=rec['tf']['translation']['x'],
                                                          y=rec['tf']['translation']['y'],
                                                          z=rec['tf']['translation']['z']),
                                  rotation=Rotation(x=rec['tf']['rotation']['x'],
                                                    y=rec['tf']['rotation']['y'],
                                                    z=rec['tf']['rotation']['z'],
                                                    w=rec['tf']['rotation']['w'])))
            sensors.append(sensor)
    properties = Properties(syncId=syncId,
                            completed=True,)
    if sensors:
        properties.Sensors = sensors
    if mapFile is not None:
        properties.Map = mapFile
    updatedMission = Mission(uuid=retrievedMission['UniqueID'],
                             executionDatetime=executionDatetime,
                             properties=properties)
    return dms.patchMission(json.loads(json.dumps(updatedMission, default=my_serializer)), token)[0]["UniqueID"]


def postFilesWithMetadata(inspTaskToMissionTask, path, response, df, ttype, token):
    """Check inspection Tasks and post csv file and metadata objects to the DMS"""
    df['inspection_task_uuid'] = df['inspection_task_uuid'].apply(lambda x: x.strip())
    checkInspectionTasks(df['inspection_task_uuid'], inspTaskToMissionTask.keys(),
                         ttype + ' file')
    df['timestamp'] = df['timestamp'].apply(timestamp_to_datetime)
    df['inspection_task_uuid'] = df['inspection_task_uuid'].apply(lambda task: inspTaskToMissionTask[task])
    out = [sub_df for _, sub_df in df.groupby('inspection_task_uuid')]
    for rec in out:
        missionTaskID = rec['inspection_task_uuid'].iloc[0]
        del rec['inspection_task_uuid']
        file = path + ttype + '_' + missionTaskID[:8] + '.csv'
        fileType = path + ttype + '.csv'
        rec.to_csv(file, index=False)
        fileID = dms.postFile(file, token)
        url = str(os.getenv("DMS_FILE_URL") + str(fileID) + '/')
        response[ttype].append({'fileMetadataID': '', 'url': url})
        metaFileID = createFileMetadata(file, fileType, missionTaskID, url, token)
        response[ttype][-1]['fileMetadataID'] = metaFileID


def postAttachmentsWithMetadata(inputFiles, path, inspTaskToMissionTask, df, response, ttype, token, case=None):
    """Check inspection Tasks and post attachment and metadata objects to the DMS"""
    df['inspection_task_uuid'] = df['inspection_task_uuid'].apply(lambda x: x.strip())
    checkInspectionTasks(df['inspection_task_uuid'], inspTaskToMissionTask.keys(),
                         ttype + ' file')
    df['timestamp'] = df['timestamp'].apply(timestamp_to_datetime)
    df['inspection_task_uuid'] = df['inspection_task_uuid'].apply(lambda task: inspTaskToMissionTask[task])
    for rec in df.iterrows():
        if rec[1]['file_name'] in inputFiles.keys():
            file = inputFiles[rec[1]['file_name']]
            fileID = dms.postFile(file, token)
            url = str(os.getenv("DMS_FILE_URL") + str(fileID) + '/')
            response['attachments'].append({'fileMetadataID': '', 'url': url})
            fileType = path + ttype + '.'
            properties = addPropertiesToScannerMeta(rec[1]) if case == Case.CART.name else None
            metaFileID = createFileMetadata(file, fileType, rec[1]['inspection_task_uuid'], url, token, properties)
            response['attachments'][-1]['fileMetadataID'] = metaFileID


def addPropertiesToScannerMeta(record):
        scannerMeta = ScannerMetadata(timestamp_to_datetime(record['timestamp']), record['p_x'], record['p_y'],
                                      record['p_z'], record['frame'])
        properties = Properties(scannerMetadata=scannerMeta)
        return properties


def postImages(inputFiles, inspTaskToMissionTask, pictures_meta_df, response, imageInfo, token):
    """Check inspection Tasks and post image and metadata objects to the DMS"""
    pictures_meta_df['inspection_task_uuid'] = pictures_meta_df['inspection_task_uuid'].apply(lambda x: x.strip())
    checkInspectionTasks(pictures_meta_df['inspection_task_uuid'], inspTaskToMissionTask.keys(),
                         MetadataType.image_metadata.value)
    pictures_meta_df['timestamp'] = pictures_meta_df['timestamp'].apply(timestamp_to_datetime)
    for rec in pictures_meta_df.iterrows():
        if rec[1]['file_name'] in inputFiles.keys():
            response_tag = 'images'
            if 'ai' in rec[1].keys() and er.is_not_empty_or_null(rec[1]['ai']) and rec[1]['ai'] is False:
                response_tag = 'other'
            file = inputFiles[rec[1]['file_name']]
            fileID = dms.postFile(file, token)
            url = str(os.getenv("DMS_FILE_URL") + str(fileID) + '/')
            response[response_tag].append({'fileMetadataID': '', 'url': url})
            rec[1]['inspection_task_uuid'] = inspTaskToMissionTask[rec[1]['inspection_task_uuid']]
            fileMetaID = createImageMetadata(file, rec[1], imageInfo, url, token)
            response[response_tag][-1]['fileMetadataID'] = fileMetaID


def checkInspectionTasks(tasks, validated_tasks, file):
    """Check if all tasks exist in validated_tasks"""
    check = all(item in validated_tasks for item in tasks)
    if not check:
        raise er.BAD_REQUEST(
            'In file:' + file + ', there are inspection task IDs that are not included in the config file!')


def createImageMetadata(file, record, imageInfo, url, token):
    """Instantiate Image Metadata Object and save it to the DMS"""
    imageMeta = ImageMetadata(timestamp_to_datetime(record['timestamp']), record['p_x'], record['p_y'], record['p_z'],
                              record['q_x'], record['q_y'], record['q_z'], record['q_w'],
                              record['distance'] if 'distance' in record.keys() else None,
                              record['mm_per_pixel'] if 'mm_per_pixel' in record.keys() else None,
                              record['forward_vector_x'] if 'forward_vector_x' in record.keys() else None,
                              record['forward_vector_y'] if 'forward_vector_y' in record.keys() else None,
                              record['forward_vector_z'] if 'forward_vector_z' in record.keys() else None,
                              record['left_vector_x'] if 'left_vector_x' in record.keys() else None,
                              record['left_vector_y'] if 'left_vector_y' in record.keys() else None,
                              record['left_vector_z'] if 'left_vector_z' in record.keys() else None,
                              record['euler_angle_z_psi'] if 'euler_angle_z_psi' in record.keys() else None,
                              record['euler_angle_x_theta'] if 'euler_angle_x_theta' in record.keys() else None,
                              record['euler_angle_z_phi'] if 'euler_angle_z_phi' in record.keys() else None,
                              record['hit_point_x'] if 'hit_point_x' in record.keys() else None,
                              record['hit_point_y'] if 'hit_point_y' in record.keys() else None,
                              record['hit_point_z'] if 'hit_point_z' in record.keys() else None,
                              record['hit_angle'] if 'hit_angle' in record.keys() else None,
                              record['spot_light'] if 'spot_light' in record.keys() else None,
                              record['flood_light'] if 'flood_light' in record.keys() else None,
                              record['temperature_celsius'] if 'temperature_celsius' in record.keys() else None,
                              record['pressure_bar'] if 'pressure_bar' in record.keys() else None,
                              record['pan'] if 'pan' in record.keys() else None,
                              record['tilt'] if 'tilt' in record.keys() else None,
                              record['zoom'] if 'zoom' in record.keys() else None,
                              record['obj_ref'] if 'obj_ref' in record.keys() else None)
    if imageInfo:
        imageMeta.Size = ImageSize(width=imageInfo['size']['width'],
                                   height=imageInfo['size']['height'])
        imageMeta.Fov = ImageFov(horizontal=imageInfo['fov']['horizontal'],
                                 vertical=imageInfo['fov']['vertical'])
    else:
        image = PIL.Image.open(file)
        width, height = image.size
        imageMeta.Fov = ImageFov(horizontal=record['horizontal_fov'],
                                 vertical=record['vertical_fov'])
        if imageMeta.Fov.Horizontal and imageMeta.Fov.Vertical:
            del imageMeta.Fov.Horizontal
            del imageMeta.Fov.Vertical
        imageMeta.Size = ImageSize(width=width,
                                   height=height)

    if 'obj_ref' in record.keys() and imageMeta.obj_ref_id == 0:
        imageMeta.obj_ref_id = None

    mime = magic.Magic(mime=True)
    properties = Properties(imageMetadata=imageMeta)
    fileMetadata = FileMetadata(url=url,
                                mimeType=str(mime.from_file(file)),
                                size=str(os.stat(file).st_size),
                                originalName=record['file_name'],
                                content=str('mRCS - Image'),
                                creationDatetime=time_to_datetime(
                                    datetime.strftime(datetime.utcnow(), '%Y_%m_%d-%H_%M_%S')),
                                properties=properties,
                                object_id=record['inspection_task_uuid'],
                                content_type=FileMetadataEnum.MissionTask.value)
    return dms.postFileMetadata(json.loads(json.dumps(fileMetadata, default=my_serializer)), token)['UniqueID']


def createFileMetadata(file, fileType, missionTaskID, url, token, properties=None):
    """Instantiate FileMetadata Object and save it to the DMS"""
    mime = magic.Magic(mime=True)
    mimeType = str(mime.from_file(file))
    ext = str(file).rsplit('.', 1)[1].lower()
    if ext == 'csv' or ext == 'tsv':
        mimeType = 'text/' + ext
    elif ext == 'pcd':
        mimeType = 'application/vnd.pcd'
    fileMetadata = FileMetadata(url=url,
                                mimeType=mimeType,
                                size=str(os.stat(file).st_size),
                                originalName=str(str(file).rsplit('/', 1)[1]),
                                content=str('mRCS - ' + str(fileType).rsplit('/', 1)[1].rsplit('.', 1)[0]),
                                creationDatetime=time_to_datetime(
                                    datetime.strftime(datetime.utcnow(), '%Y_%m_%d-%H_%M_%S')),
                                object_id=missionTaskID,
                                content_type=FileMetadataEnum.MissionTask.value)
    if properties:
        fileMetadata.Properties = properties
    res = dms.postFileMetadata(json.loads(json.dumps(fileMetadata, default=my_serializer)), token)
    return res["UniqueID"]


def mapInspectionToMissionTask(inspectionTaskUUIDs, retrievedMissionTasks, case=None):
    """Create a dictionary that maps the inspectionTask UUID with mission Task UUID"""
    inspToMissionTask = {}
    for val in retrievedMissionTasks:
        if val['InspectionTask'] not in inspectionTaskUUIDs:
            if case is not None and case.name in [Case.RISING.name, Case.CART.name]:
                continue
            else:
                raise er.BAD_REQUEST("All Inspection Tasks given must associated with a Mission Task")
        inspToMissionTask[val['InspectionTask']] = val['UniqueID']
    return inspToMissionTask


def validateInputData(mission_json, token):
    """Validate Input Data and returns Mission and Mission Tasks from the DMS"""
    validate(instance=mission_json, schema=mRCSJsonSchema)
    for sensor in mission_json['sensors']:
        validate(instance=sensor, schema=sensorSchema)
    mission, missionTasks = fetchMissionAndMissionTasks(mission_json['syncId'],
                                                        mission_json['inspectionPlanUuid'],
                                                        token)
    return mission, missionTasks


def validateBIKEInputData(mission_json, token):
    """Validate BIKE and SATELITE Input Data and fetch Mission and Mission Tasks from the DMS"""
    validate(instance=mission_json, schema=mRCSBikeJsonSchema)
    for capture in mission_json['Captures']:
        validate(instance=capture, schema=captureSchema)
    mission, missionTasks = fetchMissionAndMissionTasks(mission_json['Mission']['Sync ID'],
                                                        mission_json['Mission']['Inspection Plan UUID'],
                                                        token)
    return mission, missionTasks


def validateAEROXInputData(mission_json, token):
    """Validate AEROX Input Data and returns Mission and Mission Tasks from the DMS"""
    validate(instance=mission_json, schema=mRCSAeroxJsonSchema)
    for rec in mission_json['data']:
        validate(instance=rec, schema=AeroxDataSchema)
    mission, missionTasks = fetchMissionAndMissionTasks(mission_json['syncId'],
                                                        mission_json['inspectionPlanUuid'],
                                                        token)
    return mission, missionTasks


def fetchMissionAndMissionTasks(syncID, inspectionPlanUUID, token):
    """Fetch Mission and Mission Tasks of an Inspection Plan"""
    mission = er.exists(dms.getMissionBySyncId(syncID, token), 'Mission with that specific SyncID ')[0]
    if not mission['InspectionPlan'] == inspectionPlanUUID:
        raise er.BAD_REQUEST("Inspection Plan is not the same as the Mission Task's Inspection Plan")
    missionTasks = dms.getMissionTasksByMissionID(mission['UniqueID'], Op.missionTask, token)[0]['MissionTask']
    return mission, missionTasks


def add_to_pictures_meta_df(capture, image, pictures_meta_df, timestamp, ai):
    """Add records to dataframe - images"""
    if ai is True:
        pictures_meta_df.loc[pictures_meta_df.shape[0]] = [ai, image.replace('/', '_'),
                                                           timestamp,
                                                           capture['Inspection Task UUID'],
                                                           capture['Inspection Camera Hit Point x [m]'],
                                                           capture['Inspection Camera Hit Point z [m]'],
                                                           capture['Inspection Camera Hit Point y [m]'] * -1,
                                                           capture['Inspection Camera Quaternion real'],
                                                           capture['Inspection Camera Quaternion j'],
                                                           capture['Inspection Camera Quaternion i'],
                                                           capture['Inspection Camera Quaternion k'],
                                                           capture['Inspection Camera Forward Vector x [m]'],
                                                           capture['Inspection Camera Forward Vector y [m]'],
                                                           capture['Inspection Camera Forward Vector z [m]'],
                                                           capture['Inspection Camera Left Vector x [m]'],
                                                           capture['Inspection Camera Left Vector y [m]'],
                                                           capture['Inspection Camera Left Vector z [m]'],
                                                           capture["Inspection Camera Euler Angle Z' [deg]"],
                                                           capture["Inspection Camera Euler Angle X'' [deg]"],
                                                           capture["Inspection Camera Euler Angle Z''' [deg]"],
                                                           capture['Inspection Camera Hit Point x [m]'],
                                                           capture['Inspection Camera Hit Point y [m]'],
                                                           capture['Inspection Camera Hit Point z [m]'],
                                                           capture['Inspection Camera Hit Distance [m]'],
                                                           capture['Inspection Camera Hit Angle [deg]'],
                                                           capture['Inspection Camera Spot Light [%]'],
                                                           capture['Inspection Camera Flood Light [%]'],
                                                           capture['Inspection Camera Horizontal FOV [deg]'],
                                                           capture['Inspection Camera Vertical FOV [deg]'],
                                                           capture['Inspection Camera Temperature [Celsius]'],
                                                           capture['Inspection Camera Pressure [bar]'],
                                                           capture['Inspection Camera Pan [deg]'],
                                                           capture['Inspection Camera Tilt [deg]'],
                                                           capture['Inspection Camera Zoom [x]'],
                                                           ]
    else:
        none_list = [None] * 30
        values = [ai, image.replace('/', '_'), timestamp, capture['Inspection Task UUID']]
        values.extend(none_list)
        pictures_meta_df.loc[pictures_meta_df.shape[0]] = values


def add_to_attachment_meta_df(capture, image, attachment_df, timestamp):
    """Add records to dataframe - attachments"""
    attachment_df.loc[attachment_df.shape[0]] = [image.replace('/', '_'),
                                                 timestamp,
                                                 capture['Inspection Task UUID']
                                                 ]


def add_to_telemetry_df(capture, telemetry_df, timestamp):
    """Add records to dataframe - telemetry data"""
    telemetry_df.loc[telemetry_df.shape[0]] = [timestamp,
                                               capture['Inspection Task UUID'],
                                               capture['Robot x [m]'], capture['Robot y [m]'],
                                               capture['Robot z [m]'], capture['Robot Quaternion real'],
                                               capture['Robot Quaternion i'], capture['Robot Quaternion j'],
                                               capture['Robot Quaternion k'], capture['Robot Forward Vector x [m]'],
                                               capture['Robot Forward Vector y [m]'],
                                               capture['Robot Forward Vector z [m]'],
                                               capture['Robot Left Vector x [m]'],
                                               capture['Robot Left Vector y [m]'],
                                               capture['Robot Left Vector z [m]'],
                                               capture["Robot Euler Angle Z' [deg]"],
                                               capture["Robot Euler Angle X'' [deg]"],
                                               capture["Robot Euler Angle Z''' [deg]"]]


def add_to_ut_measurements_df(capture, thickness_file_ID, thickness_type, thickness_value_mm, timestamp,
                              ut_measurements_df, case):
    """Add records to dataframe - UT measurements"""
    if case == Case.AEROX.name:
        ut_measurements_df.loc[ut_measurements_df.shape[0]] = [timestamp,
                                                               capture['inspectionTaskUuid'],
                                                               capture['initialPosition']['x'],
                                                               capture['initialPosition']['y'],
                                                               capture['initialPosition']['z'],
                                                               capture['finalPosition']['x'],
                                                               capture['finalPosition']['y'],
                                                               capture['finalPosition']['z'],
                                                               thickness_type, thickness_value_mm, thickness_file_ID]
    else:
        ut_measurements_df.loc[ut_measurements_df.shape[0]] = [timestamp,
                                                               capture['Inspection Task UUID'],
                                                               capture['UT Probe x [m]'],
                                                               capture['UT Probe y [m]'],
                                                               capture['UT Probe z [m]'],
                                                               capture['UT Probe Quaternion real'],
                                                               capture['UT Probe Quaternion i'],
                                                               capture['UT Probe Quaternion j'],
                                                               capture['UT Probe Quaternion k'],
                                                               capture['UT Probe Forward Vector x [m]'],
                                                               capture['UT Probe Forward Vector y [m]'],
                                                               capture['UT Probe Forward Vector z [m]'],
                                                               capture['UT Probe Left Vector x [m]'],
                                                               capture['UT Probe Left Vector y [m]'],
                                                               capture['UT Probe Left Vector z [m]'],
                                                               capture["UT Probe Euler Angle Z' [deg]"],
                                                               capture["UT Probe Euler Angle X'' [deg]"],
                                                               capture["UT Probe Euler Angle Z''' [deg]"],
                                                               thickness_type, thickness_value_mm, thickness_file_ID]


def load_df(inputFiles, ext):
    """ Responsible to find the path of the file with extension given. Then, the function gathers
    all csv files living in the same folder with the aforementioned file and concentrate all values
    to one dataframe and returns it"""

    dfs = list()
    new_path = find_files_path(inputFiles, ext)
    files_lst = resolve_files(new_path)
    if len(files_lst) ==0:
        return None
    if (len(files_lst) % 2) != 0:
        raise er.BAD_REQUEST('Each FARO file should have a config file')
    for f in files_lst:
        if str(f).endswith(".csv"):
            data = pd.read_csv(inputFiles[f], names=[
                'file_name', 'timestamp', 'inspection_task_uuid', 'p_x', 'p_y', 'p_z', 'frame'])
            dfs.append(data)
    df = pd.concat(dfs, ignore_index=True)
    return df


def resolve_files(new_path):
    """Gathers all useful files inside a path"""
    files_lst = []
    if new_path != '':
        files_lst = os.listdir(new_path)
        for f in files_lst:
            if f.endswith('/') or f.endswith('DS_Store') or f.startswith('__'):
                files_lst.remove(f)
    return files_lst


def find_files_path(inputFiles, ext):
    """Finds path of the folder containing file with specific extension taking into account
     a dictionary of files and their paths given"""
    new_path = ''
    for rec in inputFiles:
        if str(rec).endswith(ext):
            new_path = str(inputFiles[rec].rsplit('/', 1)[0]) + '/'
            break
    return new_path


def createMapFileMetadata(file, fileId, response, token, missionID):
    """Post Map Object and save it to the DMS"""
    url = str(os.getenv("DMS_FILE_URL") + fileId + '/')
    fileMetadata = FileMetadata(url=url,
                                mimeType="model/stl",
                                size=str(os.stat(file).st_size),
                                originalName=str(str(file).rsplit('/', 1)[1]),
                                content=str('mRCS - Map file of the Inspection Pipe'),
                                creationDatetime=time_to_datetime(
                                    datetime.strftime(datetime.utcnow(), '%Y_%m_%d-%H_%M_%S')),
                                object_id=missionID,
                                content_type=FileMetadataEnum.Mission.value)
    fileMeta = dms.postFileMetadata(json.loads(json.dumps(fileMetadata, default=my_serializer)), token)["UniqueID"]
    response['other'].append({'fileMetadataID': fileMeta, 'url': url})
