import logging

from jsonschema import *
from ddhlApi.enum import *
from ddhlApi.enum import Operations as Op
from ddhlApi.schemas import *
from ddhlApi.utils import *
from models.mission import *
import ddhlApi.dms as dms

logger = logging.getLogger('ddhl.gRCS.helper')


def deleteObjects(response, token):
    """Delete Files and Metadata from the DMS"""
    if response:
        for rec in response['files']:
            dms.deleteFile(rec, Op.delete, token)
        for rec in response['fileMetadata']:
            dms.deleteFileMetadata(rec, Op.delete, token)
        for rec in response['missionTasks']:
            dms.deleteMissionTask(rec, Op.delete, token)
        if response['mission'] != '':
            dms.deleteMission(response['mission'], Op.delete, token)


def createFilesWithMetadata(inputFiles, response, token):
    """For each file instantiate File and FileMetadata Objects and post them to the DMS """
    for filename in inputFiles.values():
        with open(filename, 'r+') as f:
            print(filename)
            fileId = createCSVFile(f, filename, response, token)
            createFileMetadata(filename, fileId, response, token)


def validateInputData(mission_json, token):
    """Validate format of the input config json file"""
    validate(instance=mission_json, schema=missionSchema)
    inspectionTaskIDs = []
    for mt in mission_json['MissionTasks']:
        validate(instance=mt, schema=missionTaskSchema)
        inspectionTaskIDs.append(mt['InspTaskUuid'])
    er.not_exist(dms.getMissionBySyncId(mission_json['SyncId'], token), "Mission with specific SyncID ")
    evaluateInspectionTasks(mission_json['InspectionPlanUuid'], inspectionTaskIDs, token)


def evaluateInspectionTasks(plan, tasks, token):
    """Compare given Inspection Tasks with the tasks of the Inspection PLan retrieved by the DMS"""
    inspectionPlan = dms.getInspectionPlan(plan, Op.fullInspectionPlan, token)
    er.checkNotFound(inspectionPlan, "Inspection Plan does not exits. It")
    retrievesITs = []
    for it in inspectionPlan[0]['InspectionTask']:
        retrievesITs.append(it['UniqueID'])
    for task in tasks:
        if task not in retrievesITs:
            er.checkNotFound(None, 'Inspection Task of the Inspection Plan')


def createFileMetadata(file, fileId, response, token):
    """Instantiate FileMetadata Object and save it to the DMS"""
    fileMetadata = FileMetadata(url=str(os.getenv("DMS_FILE_URL") + fileId + '/'),
                                mimeType="text/" + str(file).rsplit('.', 1)[1].lower(),
                                size=str(os.stat(file).st_size),
                                originalName=str(str(file).rsplit('/', 1)[1]),
                                content=str('gRCS - ' + str(file).rsplit('/', 1)[1].rsplit('.', 1)[0]),
                                creationDatetime=time_to_datetime(datetime.strftime(datetime.utcnow(), '%Y_%m_%d-%H_%M_%S')),
                                object_id=response['mission'],
                                content_type=FileMetadataEnum.Mission.value)
    response['fileMetadata'].append(
        dms.postFileMetadata(json.loads(json.dumps(fileMetadata, default=my_serializer)), token)["UniqueID"])


def createCSVFile(f, file, response, token):
    """Post csv file to the DMS"""
    update_csv_file(f)
    fileId = dms.postFile(file, token)
    response['files'].append(fileId)
    return fileId


def createMissionTasks(mission_json, response, token):
    """Instantiate Mission Task Objects and save them to the DMS"""
    if mission_json['MissionTasks']:
        for mt in mission_json['MissionTasks']:
            missionTask = MissionTask(name=mt["Name"],
                                      executionDatetime=mt["CreationTimeStamp"],
                                      description=mt["Description"],
                                      inspectionTaskId=mt["InspTaskUuid"],
                                      inspectionTypeId=dms.getInspectionTypeByInspectionTaskID(mt["InspTaskUuid"], Op.inspectionType, token)[0]['InspectionType']['UniqueID'],
                                      missionId=response['mission'])
            response['missionTasks'].append(
                dms.postMissionTask(json.loads(json.dumps(missionTask, default=my_serializer)), token)["UniqueID"])


def createMission(mission_json, response, token):
    """Instantiate Mission Object and save it to the DMS"""
    mission = Mission(name=mission_json["Name"],
                      executionDatetime=mission_json["CreationTimeStamp"],
                      description=mission_json["Description"],
                      properties=Properties(syncId=mission_json["SyncId"]),
                      inspectionPlan=mission_json["InspectionPlanUuid"])
    response['mission'] = dms.postMission(json.loads(json.dumps(mission, default=my_serializer)), token)["UniqueID"]
