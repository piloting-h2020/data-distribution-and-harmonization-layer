import logging

import ddhlApi.dms as dms
from models.inspectionPlanvView import *
from ddhlApi.enum import Operations as Op
import json
import pandas as pd
from ddhlApi.enum import *

logger = logging.getLogger('ddhl.common.helper')


def consolidate_plans_per_asset(tasks):
    plansPerAsset = {}
    for task in tasks:
        asset = task['Asset']
        inspectionPlan_lst = [inspection['InspectionPlan'] for inspection in task['InspectionTask'] if task['InspectionTask']]
        if asset not in plansPerAsset.keys():
            plansPerAsset[asset] = inspectionPlan_lst
        else:
            plansPerAsset[asset].extend(inspectionPlan_lst)
            plansPerAsset[asset] = list(set(plansPerAsset[asset]))
    return plansPerAsset


def create_list_of_inspection_plans(plansPerAsset, token):
    res = []
    for key in plansPerAsset.keys():
        assetDto = Asset(UUID=key, name=dms.getAssetNameByID(key, Op.asset, token)[0]['Name'])
        for rec in plansPerAsset[key]:
            plan = InspectionPlanJsoToDto(dms.getInspectionPlan(rec, Op.inspectionPlan, token)[0], assetDto)
            res.append(plan)
    return json.loads(json.dumps(res, default=my_serializer))


def filter_plans_and_group_by_site(plans, request, site_id):
    df = pd.DataFrame.from_records(plans)
    df['site'] = df.apply(lambda x: x['Properties']['site'] if 'site' in x['Properties'] else 0, axis=1)
    df.drop(df[df.site == 0].index, inplace=True)
    df = df[df['site'].str.contains(Experiments(site_id).name.lower(), case=False)]
    df = replace_asset_id_with_asset(df, request)
    response = df.groupby('site').apply(lambda x: x.to_dict(orient='records'))
    response = json.loads(response.to_json())
    return response


def replace_asset_id_with_asset(df, request):
    assets = {}
    asset_ids = df['Asset'].unique().tolist()
    for rec in asset_ids:
        assets[rec] = dms.getAssetNameByID(rec, Op.asset, request.user)[0]
    df = df.replace({"Asset": assets})
    return df
