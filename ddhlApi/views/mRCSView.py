import logging

from django.http import JsonResponse
from rest_framework.views import APIView
from rabbitMQ.rabbitMQ import RabbitMQ
from ddhlApi.views.helpers.mRCSViewsHelper import *
logger = logging.getLogger('ddhl.mRCS')


class mRCSView(APIView):
    @staticmethod
    def post(request):
        case = Case[request.get_full_path().rsplit('/', 2)[1]]
        if case.name == Case.AEROX.name:
            return postAEROXMissionSpecificData(request)
        try:
            rabMQ = RabbitMQ(host=os.getenv("RABBITMQ_HOST"), port=os.getenv("RABBITMQ_PORT"),
                             queue=os.getenv(case.value['queue'][0]), heartbeat=int(os.getenv("RABBITMQ_HEARTBEAT")))
        except:
            logger.error("No connection to rabbit mq")
            return JsonResponse("No connection to rabbit mq", status=er.INTERNAL_SERVER_ERROR.status_code, safe=False)
        if case.name in [Case.BIKE.name, Case.SATELITE.name]:
            return postUTMissionSpecificData(request, rabbitMQ=rabMQ, case=case)
        return postMissionSpecificData(request, rabbitMQ=rabMQ, case=case)


def postMissionSpecificData(request, path=None, rabbitMQ=None, case=None):
    """Post Mission Specific Data for AEROCAM, CART, TTDRONE, VIADDRONE, ETH, AEROX, RISING robots"""
    response = {'mission': "", 'images': [], 'localization_telemetry': [], 'gps_telemetry': [], 'installation_log': [],
                'haptic_data': [], 'objects_found': [], 'attachments': [], 'gauge_readings': []}
    try:
        logger.info("Start - Post Mission Specific Data: " + str(request.get_full_path()))
        logger.info("User: " + str(request.user.username))
        path, inputFiles = checkZipFIleAndUnzip(request)
        # Input: Mission & Mission Tasks
        mission_json = loadMission(inputFiles)
        # Retrieved Mission & Mission Tasks
        retrievedMission, retrievedMissionTasks = validateInputData(mission_json, request.user)
        # Map InspectionTask with MissionTask
        inspTaskToMissionTask = mapInspectionToMissionTask(mission_json['inspectionTaskUuids'], retrievedMissionTasks, case)
        # Post mRCS Files
        postmRCSFiles(inputFiles, inspTaskToMissionTask, mission_json, path, response, request.user, case.name)
        response['mission'] = patchMission(mission_json, retrievedMission, request.user)
        # Publish message to RabbitMQ
        if len(response['installation_log']) == 0:
            publishRabbitMQMessage(response, rabbitMQ, case, mission_json['inspectionPlanUuid'], request.user)
        logger.info("Success Response: " + str(response))
        logger.info("End - Post Mission Specific Data: " + str(request.get_full_path()))
        return JsonResponse(response, safe=False)
    except (er.NOT_FOUND, er.BAD_REQUEST) as error:
        logger.info("Post Mission Specific Data - Error: " + str(error))
        deleteObjects(response, request.user)
        return JsonResponse(str(error.args[0]), status=error.status_code, safe=False)
    except exceptions.ValidationError as error:
        logger.info("Post Mission Specific Data - Error: " + str(error))
        return JsonResponse(error.message, status=er.BAD_REQUEST.status_code, safe=False)
    except:
        deleteObjects(response, request.user)
        return JsonResponse("Something went wong!", status=er.INTERNAL_SERVER_ERROR.status_code, safe=False)
    finally:
        remove_folder(path) if path is not None else None


def postUTMissionSpecificData(request, path=None, rabbitMQ=None, case=None):
    """Post Mission Specific Data for BIKE and SATELITE robot"""
    response = {'mission': "", 'images': [], 'localization_telemetry': [], 'UT_measurements': [],  'UT_thickness': [],
                'other': [], 'attachments': []}
    try:
        logger.info("Start - Post Mission Specific Data: " + str(request.get_full_path()))
        logger.info("User: " + str(request.user.username))
        path, inputFiles = checkZipFIleAndUnzip(request)
        # Input: Mission & Mission Tasks
        mission_json = loadMission(inputFiles)
        # Retrieved Mission & Mission Tasks
        retrievedMission, retrievedMissionTasks = validateBIKEInputData(mission_json, request.user)
        # Map InspectionTask with MissionTask
        inspectionTaskUUIDs = set([capture['Inspection Task UUID'] for capture in mission_json['Captures']])
        inspTaskToMissionTask = mapInspectionToMissionTask(inspectionTaskUUIDs, retrievedMissionTasks)
        # Post mRCS Files
        postBIKEmRCSFiles(inputFiles, inspTaskToMissionTask, mission_json, path,
                          response, request.user, case, retrievedMission['UniqueID'])
        response['mission'] = patchMission(mission_json['Mission'], retrievedMission, request.user)
        # Publish message to RabbitMQ
        publishRabbitMQMessage(response, rabbitMQ, case)
        logger.info("Success Response: " + str(response))
        logger.info("End - Post Mission Specific Data: " + str(request.get_full_path()))
        return JsonResponse(response, safe=False)
    except (er.NOT_FOUND, er.BAD_REQUEST) as error:
        logger.info("Post BIKE Mission Specific Data - Error: " + str(error))
        deleteObjects(response, request.user)
        return JsonResponse(str(error.args[0]), status=error.status_code, safe=False)
    except exceptions.ValidationError as error:
        logger.info("Post BIKE Mission Specific Data - Error: " + str(error))
        return JsonResponse(error.message, status=er.BAD_REQUEST.status_code, safe=False)
    except:
        deleteObjects(response, request.user)
        return JsonResponse("Something went wong!", status=er.INTERNAL_SERVER_ERROR.status_code, safe=False)
    finally:
        remove_folder(path) if path is not None else None


def postAEROXMissionSpecificData(request, path=None):
    """Post Mission Specific Data for AEROX robot"""
    response = {'mission': "", 'UT_measurements': [],  'UT_thickness': []}
    try:
        logger.info("Post AEROX Mission Specific Data")
        logger.info("User: " + str(request.user.username))
        path, inputFiles = checkZipFIleAndUnzip(request)
        # Input: Mission & Mission Tasks
        mission_json = loadMission(inputFiles)
        # Retrieved Mission & Mission Tasks
        retrievedMission, retrievedMissionTasks = validateAEROXInputData(mission_json, request.user)
        # Map InspectionTask with MissionTask
        inspectionTaskUUIDs = set([rec['inspectionTaskUuid'] for rec in mission_json['data']])
        inspTaskToMissionTask = mapInspectionToMissionTask(inspectionTaskUUIDs, retrievedMissionTasks)
        # Post mRCS Files
        postAEROXmRCSFiles(inputFiles, inspTaskToMissionTask, mission_json, path, response, request.user)
        response['mission'] = patchMission(mission_json, retrievedMission, request.user)
        logger.info("Success Response: " + str(response))
        logger.info("End - Post Mission Specific Data: " + str(request.get_full_path()))
        return JsonResponse(response, safe=False)
    except (er.NOT_FOUND, er.BAD_REQUEST) as error:
        logger.info("Post AEROX Mission Specific Data - Error: " + str(error))
        deleteObjects(response, request.user)
        return JsonResponse(str(error.args[0]), status=error.status_code, safe=False)
    except exceptions.ValidationError as error:
        logger.info("Post AEROX Mission Specific Data - Error: " + str(error))
        return JsonResponse(error.message, status=er.BAD_REQUEST.status_code, safe=False)
    except:
        deleteObjects(response, request.user)
        return JsonResponse("Something went wong!", status=er.INTERNAL_SERVER_ERROR.status_code, safe=False)
    finally:
        remove_folder(path) if path is not None else None
