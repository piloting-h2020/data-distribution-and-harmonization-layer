import uuid
from datetime import datetime
from zipfile import ZipFile
import os
import shutil
import pandas as pd
import ddhlApi.errorHandling as er
import json


def timestamp_to_datetime(value):
    """Convert timestamp (e.g. 1637582670.62900) to UTC datetime"""
    try:
        datetime.strptime(str(value), "%Y-%m-%dT%H:%M:%SZ")
        return value
    except ValueError:
        return datetime.utcfromtimestamp(value).strftime("%Y-%m-%dT%H:%M:%SZ")


def time_to_datetime(value):
    d = datetime.strptime(value, "%Y_%m_%d-%H_%M_%S")
    return datetime.utcfromtimestamp(d.timestamp()).strftime("%Y-%m-%dT%H:%M:%SZ")


def time_date_to_datetime(value):
    try:
        d = datetime.strptime(value, "%Y-%m-%d %H:%M:%S")
        return datetime.utcfromtimestamp(d.timestamp()).strftime("%Y-%m-%dT%H:%M:%SZ")
    except ValueError:
        return value


def remove_empty_properties(json):
    for key, value in json.items():
        if value is None or str(value) == '':
            del json[key]


def dir_empty(dir_path):
    """ Check if a folder is empty"""
    return not next(os.scandir(dir_path), None)


def checkZipFIleAndUnzip(request):
    """Function that take as input a zip file, perform unzip and flatten all files in a list"""
    er.checkNotFound(request.FILES, 'Files in a zip file')
    return unzip_file(request.FILES['files'], str(request.path).rsplit('/')[-2])


def unzip_file(file, caseStr):
    """ Create new folder and unzip files
        Input: zipped file
        Output: folder path and a list of files included in the folder"""
    res = {}
    with ZipFile(file, 'r') as z:
        rid = str(uuid.uuid4())
        filePath = './tempFiles/'
        while os.path.isdir(filePath + rid):
            rid = str(uuid.uuid4())
        path = filePath + rid + '/'
        for ele in z.namelist():
            if not ele.endswith('/') and not ele.endswith('DS_Store') and not ele.startswith('__'):
                temp = ele.split('/')
                if (caseStr == 'BIKE' or caseStr == 'SATELITE') and len(temp) > 1 and temp[-2].startswith('capture'):
                    res[ele.split('/')[-2] + '_' + ele.split('/')[-1]] = path + ele
                elif caseStr == 'RISING' and len(temp) > 2:
                    temp.pop(0)
                    temp = "_".join(temp)
                    res[temp] = path + ele
                else:
                    res[ele.rsplit('/')[-1]] = path + ele
        er.checkNotFound(res, 'Files in a zip file')
        os.makedirs(filePath + rid)
        z.extractall(filePath + rid)
        return path, res


def remove_folder(folder_path):
    """ Delete a folder """
    try:
        shutil.rmtree(folder_path)
    except OSError as e:
        print("Error: %s - %s." % (e.filename, e.strerror))


def update_csv_file(f):
    """Responsible to load csv to dataframe, erase special characters and blanks from headers, update timestamp \
    values and save the file"""

    df = pd.read_csv(f)
    df.columns = df.columns.str.replace('[#,@,&]', '').str.replace(' ', '')
    for s in df.columns.values:
        if "timestamp" in s:
            df[s] = df[s].apply(timestamp_to_datetime)
    df.to_csv(f.name, index=False)


def loadMission(inputFiles):
    """ Load Mission from config file"""
    config_json = [file for file in inputFiles.keys() if file.endswith('.json')]
    er.checkNotFound(config_json, "Config.json")
    with open(inputFiles[config_json[0]]) as f:
        mission_json = json.loads(f.read())
        del inputFiles[config_json[0]]
    return mission_json
