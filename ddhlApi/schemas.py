missionSchema = {
    "type": "object",
    "properties": {
        "Description": {
            "type": "string",
            "maxLength": 50
        },
        "InspectionPlanUuid": {
            "type": "string",
            "pattern": "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
        },
        "MissionTasks": {
            "type": "array",
            "minItems": 1
        },
        "Name": {
            "type": "string",
            "minLength": 1,
            "maxLength": 50
        },
        "SyncId": {"type": "integer"},
        "CreationTimeStamp": {"type": "string"},
        "LastUpdateTimeStamp": {"type": "string"}
    },
    "required": ["InspectionPlanUuid", "SyncId", "MissionTasks", "Name", "CreationTimeStamp"],
}

missionTaskSchema = {
    "type": "object",
    "properties": {
        "Description": {
            "type": "string",
            "maxLength": 50
        },
        "InspTaskUuid": {
            "type": "string",
            "pattern": "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
        },
        "LastUpdateTimeStamp": {"type": "string"},
        "Name": {
            "type": "string",
            "minLength": 1,
            "maxLength": 50
        },
        "CreationTimeStamp": {"type": "string"}
    },
    "required": ["InspTaskUuid", "Name", "CreationTimeStamp"]
}

mRCSJsonSchema = {
    "type": "object",
    "properties": {
        "startDate": {"type": "string"},
        "endDate": {"type": "string"},
        "syncId": {"type": "integer"},
        "uuid": {
            "type": "string",
            "pattern": "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
        },
        "inspectionPlanUuid": {
            "type": "string",
            "pattern": "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
        },
        "inspectionTaskUuids": {
            "type": "array",
            "items": {
                "type": "string",
                "pattern": "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
            },
            "minItems": 1
        },
        "type": {"type": "string"},
        "map": {"type": "string"},
        "files": {
            "type": "object",
            "properties": {
                "pictures_metadata": {"type": "string"},
                "pictures_folder": {"type": "string"},
                "telemetry_data": {"type": "string"},
                "gps_data": {"type": "string"},
                "installation_log": {"type": "string"},
                "haptic_data": {"type": "string"},
            },
            "required": ["telemetry_data"],
        },
        "sensors": {
            "type": "array",
            "minItems": 1
        },
        "pictures": {
            "type": "object",
            "properties": {
                "folder": {"type": "string"},
                "sensor_uuid": {
                    "type": "string",
                    "pattern": "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
                },
                "format": {"type": "string"},
                "size": {
                    "type": "object",
                    "properties": {
                        "width": {"type": "integer"},
                        "height": {"type": "integer"},
                    },
                    "required": ["width", "height"],
                },
                "fov": {
                    "type": "object",
                    "properties": {
                        "horizontal": {"type": "number"},
                        "vertical": {"type": "number"},
                    },
                    "required": ["horizontal", "vertical"],
                }
            },
            "required": ["format", "size", "fov"],
        },
    },
    "required": ["startDate", "endDate", "syncId", "uuid", "inspectionPlanUuid", "inspectionTaskUuids", "type", "map",
                 "files", "sensors"],
}


mRCSBikeJsonSchema = {
    "type": "object",
    "properties": {
        "Mission": {
            "type": "object",
            "properties": {
                "Inspection Start Date": {"type": "string"},
                "Sync ID": {"type": "integer"},
                "Inspection Plan UUID": {
                    "type": "string",
                    "pattern": "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
                },
                # Map
            },
            "required": ["Inspection Start Date", "Sync ID", "Inspection Plan UUID"],
        },
        "Captures": {
            "type": "array",
            "minItems": 1
        }
    },
    "required": ["Mission", "Captures"],
}

mRCSAeroxJsonSchema = {
    "type": "object",
    "properties": {
        "startDate": {"type": "string"},
        "endDate": {"type": "string"},
        "syncId": {"type": "integer"},
        "uuid": {
            "type": "string",
            "pattern": "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
        },
        "inspectionPlanUuid": {
            "type": "string",
            "pattern": "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
        },
        "data": {
            "type": "array",
            "minItems": 1
        },
        "sensors": {
            "type": "array",
            "minItems": 1
        }
    },
    "required": ["startDate", "syncId", "uuid", "inspectionPlanUuid"],
}


AeroxDataSchema = {
    "type": "object",
    "properties": {
        "timestamp": {"type": "string"},
        "inspectionTaskUuid": {
            "type": "string",
            "pattern": "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
        },
        "attachment": {"type": "string"},
        "initialPosition": {
            "type": "object",
            "properties": {
                "x": {"type": "number"},
                "y": {"type": "number"},
                "z": {"type": "number"},
            },
            "required": ["x", "y", "z"]
        },
        "finalPosition": {
            "type": "object",
            "properties": {
                "x": {"type": "number"},
                "y": {"type": "number"},
                "z": {"type": "number"},
            },
            "required": ["x", "y", "z"]
        }
    },
    "required": ["timestamp", "inspectionTaskUuid", "attachment", "initialPosition", "finalPosition"],
}


captureSchema = {
    "type": "object",
    "properties": {
        "Capture": {"type": "integer"},
        "Capture Type": {"type": "string"},
        "Inspection Task UUID": {
            "type": "string",
            "pattern": "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
        },
        "Capture Title": {"type": "string"},
        "System Time": {"type": "string"},
        "Robot x [m]": {"type": ["number", "null"]},
        "Robot y [m]": {"type": ["number", "null"]},
        "Robot z [m]": {"type": ["number", "null"]},
        "Robot Quaternion real": {"type": ["number", "null"]},
        "Robot Quaternion i": {"type": ["number", "null"]},
        "Robot Quaternion j": {"type": ["number", "null"]},
        "Robot Quaternion k": {"type": ["number", "null"]},
        "Robot Forward Vector x [m]": {"type": ["number", "null"]},
        "Robot Forward Vector y [m]": {"type": ["number", "null"]},
        "Robot Forward Vector z [m]": {"type": ["number", "null"]},
        "Robot Left Vector x [m]": {"type": ["number", "null"]},
        "Robot Left Vector y [m]": {"type": ["number", "null"]},
        "Robot Left Vector z [m]": {"type": ["number", "null"]},
        "Robot Euler Angle Z' [deg]": {"type": ["number", "null"]},
        "Robot Euler Angle X'' [deg]": {"type": ["number", "null"]},
        "Robot Euler Angle Z''' [deg]": {"type": ["number", "null"]},
        "Inspection Camera": {"type": "string"},
        "Inspection Camera Pan [deg]": {"type": ["number", "null"]},
        "Inspection Camera Tilt [deg]": {"type": ["number", "null"]},
        "Inspection Camera Zoom [x]": {"type": ["number", "null"]},
        "Inspection Camera x [m]": {"type": ["number", "null"]},
        "Inspection Camera y [m]": {"type": ["number", "null"]},
        "Inspection Camera z [m]": {"type": ["number", "null"]},
        "Inspection Camera Quaternion real": {"type": ["number", "null"]},
        "Inspection Camera Quaternion i": {"type": ["number", "null"]},
        "Inspection Camera Quaternion j": {"type": ["number", "null"]},
        "Inspection Camera Quaternion k": {"type": ["number", "null"]},
        "Inspection Camera Forward Vector x [m]": {"type": ["number", "null"]},
        "Inspection Camera Forward Vector y [m]": {"type": ["number", "null"]},
        "Inspection Camera Forward Vector z [m]": {"type": ["number", "null"]},
        "Inspection Camera Left Vector x [m]": {"type": ["number", "null"]},
        "Inspection Camera Left Vector y [m]": {"type": ["number", "null"]},
        "Inspection Camera Left Vector z [m]": {"type": ["number", "null"]},
        "Inspection Camera Euler Angle Z' [deg]": {"type": ["number", "null"]},
        "Inspection Camera Euler Angle X'' [deg]": {"type": ["number", "null"]},
        "Inspection Camera Euler Angle Z''' [deg]": {"type": ["number", "null"]},
        "Inspection Camera Hit Point x [m]": {"type": ["number", "null"]},
        "Inspection Camera Hit Point y [m]": {"type": ["number", "null"]},
        "Inspection Camera Hit Point z [m]": {"type": ["number", "null"]},
        "Inspection Camera Hit Distance [m]": {"type": ["number", "null"]},
        "Inspection Camera Hit Angle [deg]": {"type": ["number", "null"]},
        "Inspection Camera Spot Light [%]": {"type": ["number", "null"]},
        "Inspection Camera Flood Light [%]": {"type": ["number", "null"]},
        "Inspection Camera Horizontal FOV [deg]": {"type": ["number", "null"]},
        "Inspection Camera Vertical FOV [deg]": {"type": ["number", "null"]},
        "Inspection Camera Temperature [Celsius]": {"type": ["number", "null"]},
        "Inspection Camera Pressure [bar]": {"type": ["number", "null"]},
        "UT Probe x [m]": {"type": ["number", "null"]},
        "UT Probe y [m]": {"type": ["number", "null"]},
        "UT Probe z [m]": {"type": ["number", "null"]},
        "UT Probe Quaternion real": {"type": ["number", "null"]},
        "UT Probe Quaternion i": {"type": ["number", "null"]},
        "UT Probe Quaternion j": {"type": ["number", "null"]},
        "UT Probe Quaternion k": {"type": ["number", "null"]},
        "UT Probe Forward Vector x [m]": {"type": ["number", "null"]},
        "UT Probe Forward Vector y [m]": {"type": ["number", "null"]},
        "UT Probe Forward Vector z [m]": {"type": ["number", "null"]},
        "UT Probe Left Vector x [m]": {"type": ["number", "null"]},
        "UT Probe Left Vector y [m]": {"type": ["number", "null"]},
        "UT Probe Left Vector z [m]": {"type": ["number", "null"]},
        "UT Probe Euler Angle Z' [deg]": {"type": ["number", "null"]},
        "UT Probe Euler Angle X'' [deg]": {"type": ["number", "null"]},
        "UT Probe Euler Angle Z''' [deg]": {"type": ["number", "null"]},
        "UT CH_A Thickness [mm]": {"type": ["number", "null"]},
        "Inspection Camera Image": {"type": ["string", "null"]},
        "Navigation Cameras Images": {"type": ["string", "null"]},
        "Attachments": {"type":  ["string", "null"]}
    },
    "required": ["Capture", "Capture Type", "Inspection Task UUID", "System Time", "Inspection Camera Image", "Navigation Cameras Images", "Attachments"],
}


sensorSchema = {
    "type": "object",
    "properties": {
        "name": {"type": "string"},
        "uuid": {
            "type": "string",
            "pattern": "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
        },
        "description": {"type": "string"},
        "tf": {
            "type": "object",
            "properties": {
                "translation": {
                    "type": "object",
                    "properties": {
                        "x": {"type": "number"},
                        "y": {"type": "number"},
                        "z": {"type": "number"},
                    },
                    "required": ["x", "y", "z"]
                },
                "rotation": {
                    "type": "object",
                    "properties": {
                        "x": {"type": "number"},
                        "y": {"type": "number"},
                        "z": {"type": "number"},
                        "w": {"type": "number"},
                    },
                    "required": ["x", "y", "z", "w"]
                }
            }
        }
    },
    "required": ["name", "uuid", "description", "tf"]
}


predictionSchema = {
    "type": "array",
    "minItems": 1,
    "items": {
        "type": "object",
        "properties": {
            "fileMetadataID": {
                "type": "string",
                "pattern": "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
            },
            "algoType": {
                "type": "integer",
                "minimum": 1,
                "maximum": 4
            },
            "regions": {
                "type": "array",
            },
        },
        "required": ["fileMetadataID", "regions"]
    }
}