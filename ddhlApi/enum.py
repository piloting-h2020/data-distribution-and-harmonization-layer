from enum import Enum


class FileMetadataEnum(Enum):
    Asset = 1
    InspectionPlan = 4
    MissionTask = 12
    ReportDefinition = 10
    Mission = 7
    InspectionTask = 5


class Experiments(Enum):
    VIADUCT = 1
    REFINERY = 2
    TUNNEL = 3


class Sites(Enum):
    VIADUCT = ['Viaduct Arroyo Jevar', 'Viaduct Arroyo Espinazo']
    REFINERY = ['Refinery Chevron Oronite']
    TUNNEL = ['Tunnel Big Cut Drainage', 'Tunnel Metsovo']


class Operations(Enum):
    inspectionPlan = '/'
    file = '/'
    asset = '/'
    fullInspectionPlan = '/All/'
    fullInspectionTask = '/All/'
    payload = '/'
    allInspectionPlans = '/'
    assetOfInspectionPlan = '/Asset/'
    inspectionType = '/InspectionType/'
    inspectionTask = '/InspectionTask/'
    fileMetadata = '/'
    missionTask = '/MissionTask/'
    delete = '/'
    mission = '/Mission/'
    fileMetadataOfInspectionPlan = '/FileMetadata/'


class MetadataType(Enum):
    localization = 'localization_telemetry'
    gps = 'gps_telemetry'
    installation_log = 'installation_log'
    haptic_data = 'haptic_data'
    gauge_readings = 'gauge_readings'
    image_metadata = 'pictures_metadata'
    UT_thickness = 'ut_thickness'
    UT_measurements = 'UT_measurements'
    objects_found = 'objects_found'
    attachment = 'attachments'
    laser_scanner_metadata = 'laser_scanner_metadata'


class AlgoType(Enum):
    YOLOv5 = 1
    USE = 2
    MaskRCNN = 3
    ICCS = 4


class Case(Enum):
    AEROX = {'id': 1, 'queue': ['RABBITMQ_REFINERY_QUEUE']}
    HYBRID = {'id': 2, 'queue': ['RABBITMQ_REFINERY_QUEUE']}
    BIKE = {'id': 3, 'queue': ['RABBITMQ_REFINERY_QUEUE']}
    ETH = {'id': 4, 'queue': ['RABBITMQ_REFINERY_QUEUE']}
    RISING = {'id': 5, 'queue': ['RABBITMQ_REFINERY_QUEUE']}
    AEROCAM = {'id': 6, 'queue': ['RABBITMQ_VIADUCT_QUEUE']}
    Installation = {'id': 7, 'queue': ['RABBITMQ_VIADUCT_QUEUE']}
    TTDRONE = {'id': 8, 'queue': ['RABBITMQ_TUNNEL_QUEUE']}
    CART = {'id': 9, 'queue': ['RABBITMQ_TUNNEL_QUEUE']}
    VIAD_DRONE = {'id': 10, 'queue': ['RABBITMQ_VIADUCT_QUEUE']}
    SATELITE = {'id': 11, 'queue': ['RABBITMQ_REFINERY_QUEUE']}


class CaptureType(Enum):
    visual = 'Cameras'
    ut = 'UT'
    attachment = 'Attachments'


class AITags(Enum):
    ai = 'ai'
    not_ai = 'not_ai'

    @classmethod
    def values(cls):
        return [member.value for member in cls]


class CameraType(Enum):
    inspection = {'name': 'Inspection Camera Image', 'tag': AITags.ai}
    navigation = {'name': 'Navigation Cameras Images', 'tag': AITags.ai}
    screenshot = {'name': 'Screenshot Images', 'tag': AITags.not_ai}
    imported = {'name': 'Imported Images', 'tag': AITags.not_ai}
    ut = {'name': 'UT Images', 'tag': AITags.not_ai}
