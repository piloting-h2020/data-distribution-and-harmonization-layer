import datetime
import logging

import requests
from dotenv import load_dotenv

from authentication import update_token
from ddhlApi.enum import Operations as Op
from ddhlApi.utils import *

load_dotenv()
logger = logging.getLogger('ddhl.dms')
headers = {'Content-Type': 'application/json', 'authorization': ''}
params = ['UniqueID', 'Name', 'Description', 'CreationDatetimeFrom', 'CreationDatetimeTo',
          'UpdateDatetimeFrom', 'UpdateDatetimeTo', 'Properties']

urlDict = {'url': '', 'uuid': '', 'op': '', 'param': ''}


def getInspectionPlan(plan_id, op, token):
    urlDict.update(url=os.getenv("DMS_INSPECTION_PLAN_URL"), uuid=plan_id, op=op.value, param='')
    url = createUrl()
    logger.info("DMS: Get Inspection Plan - URL: " + url)
    return getFromDMS(url, token)


def getFileByID(file_id, op, token):
    urlDict.update(url=os.getenv("DMS_FILE_URL"), uuid=file_id, op=op.value, param='')
    url = createUrl()
    logger.info("DMS: Get File - URL: " + url)
    return getFilesFromDMS(url, token)


def getMissionsByInspectionPlan(plan_id, op, token):
    urlDict.update(url=os.getenv("DMS_INSPECTION_PLAN_URL"), uuid=plan_id, op=op.value, param='')
    url = createUrl()
    logger.info("DMS: Get Missions of an Inspection Plan - URL: " + url)
    return getFromDMS(url, token)


def getFullInspectionTasks(taskList, op, token):
    response = []
    for taskId in taskList:
        task = getFullInspectionTask(taskId, op, token)[0]
        for robotic in task['RoboticSystem']:
            for index, value in enumerate(robotic['Payload']):
                robotic['Payload'][index] = getRCSPayload(value, Op.payload, token)[0]
        response.append(task)
    return response


def getAssetNameByID(asset_id, op, token):
    urlDict.update(url=os.getenv("DMS_ASSET_URL"), uuid=asset_id, op=op.value, param='')
    url = createUrl()
    logger.info("DMS: Get Asset By ID - URL: " + url)
    return getFromDMS(url, token)


def getRCSPayload(payloadId, op, token):
    urlDict.update(url=os.getenv("DMS_PAYLOAD_URL"), uuid=payloadId, op=op.value, param='')
    url = createUrl()
    logger.info("DMS: Get RCS Payload - URL: " + url)
    return getFromDMS(url, token)


def getFileMetadata(metaId, op, token):
    urlDict.update(url=os.getenv("DMS_FILE_METADATA_URL"), uuid=metaId, op=op.value, param='')
    url = createUrl()
    logger.info("DMS: Get File Metadata - URL: " + url)
    return getFromDMS(url, token)


def getFullInspectionTask(task_id, op, token):
    urlDict.update(url=os.getenv("DMS_INSPECTION_TASK_URL"), uuid=task_id, op=op.value, param='')
    url = createUrl()
    logger.info("DMS: Get Full Inspection Task - URL: " + url)
    return getFromDMS(url, token)


def getMissionBySyncId(sync_id, token):
    url = addParamToUrl('%7B%22SyncId%22%3A%20' + str(sync_id) + '%7D', "Properties", os.getenv("DMS_MISSION_URL"))
    logger.info("DMS: Get Mission By SyncID - URL: " + url)
    return getFromDMS(url, token)


def getInspectionPlanBySiteLiteral(sites, token):
    res = {}
    assets = {}
    for site in sites:
        literal = str(site).replace(" ", "%20")
        url = addParamToUrl('%7B%22site%22%3A%20%22' + str(literal) + '%22%7D', "Properties", os.getenv("DMS_INSPECTION_PLAN_URL"))
        logger.info("DMS: Get Mission By SyncID - URL: " + url)
        res[site] = getFromDMS(url, token)
        for rec in res[site]:
            if not rec['Asset'] in assets.keys():
                assets[rec['Asset']] = getAssetNameByID(rec['Asset'], Op.asset, token)[0]
            rec['Asset'] = assets[rec['Asset']]
    return res


def getMissionTasksByMissionID(mission_id, op, token):
    urlDict.update(url=os.getenv("DMS_MISSION_URL"), uuid=mission_id, op=op.value, param='')
    url = createUrl()
    logger.info("DMS: Get Mission By Mission Task ID - URL: " + url)
    return getFromDMS(url, token)


def postMission(mission, token):
    urlDict.update(url=os.getenv("DMS_MISSION_URL"), uuid='', op='', param='')
    url = createUrl()
    logger.info("DMS: Post Mission - URL: " + url + " - Body: " + str(mission))
    return postToDMS(url, token, mission, None)


def patchMission(mission, token):
    urlDict.update(url=os.getenv("DMS_MISSION_URL"), uuid='', op='', param='')
    url = createUrl()
    logger.info("DMS: Patch Mission - URL: " + url + " - Body: " + str(mission))
    return patchObject(url, token, mission)


def patchFileMetadata(fileMeta, token):
    urlDict.update(url=os.getenv("DMS_FILE_METADATA_URL"), uuid='', op='', param='')
    url = createUrl()
    logger.info("DMS: Patch File Metadata - URL: " + url + " - Body: " + str(fileMeta))
    return patchObject(url, token, fileMeta)


def patchInspectionPlan(plan, token):
    urlDict.update(url=os.getenv("DMS_INSPECTION_PLAN_URL"), uuid='', op='', param='')
    url = createUrl()
    logger.info("DMS: Patch Inspection Plan - URL: " + url + " - Body: " + str(plan))
    return patchObject(url, token, plan)


def postMissionTask(missionTask, token):
    urlDict.update(url=os.getenv("DMS_MISSION_TASK_URL"), uuid='', op='', param='')
    url = createUrl()
    logger.info("DMS: Post Mission Task - URL: " + url + " - Body: " + str(missionTask))
    return postToDMS(url, token, missionTask, None)


def postFile(file, token):
    urlDict.update(url=os.getenv("DMS_FILE_URL"), uuid='', op='', param='')
    url = createUrl()
    logger.info("DMS: Post File - URL: " + url + " - Body: " + str(file))
    return postToDMS(url, token, None, file)


def deleteFile(file_id, op, token):
    urlDict.update(url=os.getenv("DMS_FILE_URL"), uuid=file_id, op=op.value, param='')
    url = createUrl()
    logger.info("DMS: Delete File - URL: " + url)
    deleteObject(url, token)


def deleteFileMetadata(meta_id, op, token):
    urlDict.update(url=os.getenv("DMS_FILE_METADATA_URL"), uuid=meta_id, op=op.value, param='')
    url = createUrl()
    logger.info("DMS: Delete File Metadata - URL: " + url)
    deleteObject(url, token)


def deleteMissionTask(task_id, op, token):
    urlDict.update(url=os.getenv("DMS_MISSION_TASK_URL"), uuid=task_id, op=op.value, param='')
    url = createUrl()
    logger.info("DMS: Delete Mission Task - URL: " + url)
    deleteObject(url, token)


def deleteMission(mission_id, op, token):
    urlDict.update(url=os.getenv("DMS_MISSION_URL"), uuid=mission_id, op=op.value, param='')
    url = createUrl()
    logger.info("DMS: Delete Mission - URL: " + url)
    deleteObject(url, token)


def postFileMetadata(fileMetadata, token):
    urlDict.update(url=os.getenv("DMS_FILE_METADATA_URL"), uuid='', op='', param='')
    url = createUrl()
    logger.info("DMS: Post File Metadata - URL: " + url + " - Body: " + str(fileMetadata))
    return postToDMS(url, token, fileMetadata, None)


def getAllInspectionPlans(request, token):
    url = createUrlWithCriteria(request, os.getenv("DMS_INSPECTION_PLAN_URL"))
    return getFromDMS(url, token)


def getAllInspectionLocations(request, token):
    urlDict.update(url=os.getenv("DMS_INSPECTION_LOCATION_URL"))
    url = createUrl()
    logger.info("DMS: Get All Inspection Locations - URL: " + url)
    return getFromDMS(url, token)


def getAllInspectionTasksByLocationID(locationId, op, token):
    urlDict.update(url=os.getenv("DMS_INSPECTION_LOCATION_URL"), uuid=locationId, op=op.value, param='')
    url = createUrl()
    logger.info("DMS: Get All Inspection Tasks by Location ID - URL: " + url)
    return getFromDMS(url, token)


def getInspectionTypeByInspectionTaskID(taskId, op, token):
    urlDict.update(url=os.getenv("DMS_INSPECTION_TASK_URL"), uuid=taskId, op=op.value, param='')
    url = createUrl()
    logger.info("DMS: Get Inspection Type By Inspection Task ID - URL: " + url)
    return getFromDMS(url, token)


def getFromDMS(url, token):
    """ Responsible to check if authorization token is alive and retrieve information from the DMS"""

    # token.expirationDate = datetime.utcnow()
    if not isTokenValid(token.expirationDate):
        token = update_token(token)

    headers['authorization'] = token.token_type + ' ' + token.access_token
    r = requests.get(url=url, headers=headers)
    if r.status_code != 200:
        logger.error("DMS Error " + ' url: ' + url + ' code: ' + str(r.status_code))
        r.raise_for_status()
    return r.json()


def getFilesFromDMS(url, token):
    if not isTokenValid(token.expirationDate):
        token = update_token(token)

    headers['authorization'] = token.token_type + ' ' + token.access_token
    r = requests.get(url=url, headers=headers)
    if r.status_code == 404:
        er.exists('', 'File does ')
    if r.status_code != 200:
        logger.error("DMS Error " + ' url: ' + url + ' code: ' + str(r.status_code))
        r.raise_for_status()
    return r


def postToDMS(url, token, body, file):
    """ Responsible to check if authorization token is alive and post information to the DMS"""
    global r
    if not isTokenValid(token.expirationDate):
        token = update_token(token)

    headers['authorization'] = token.token_type + ' ' + token.access_token
    headers['Content-Type'] = "application/json"
    if body:
        r = requests.post(url=url, headers=headers, json=body)
    elif file:
        del headers['Content-Type']
        if str(file).endswith('.csv'):
            fileName = str(file).rsplit('/', 1)[1]
            files = [('file', (fileName, open(file, 'rb'), 'text/csv'))]
            r = requests.post(url=url, files=files, headers=headers, data={})
        else:
            files = {'file': open(file, 'rb')}
            r = requests.post(url=url, files=files, headers=headers, verify=False)

    if r.status_code not in [200, 201]:
        logger.error("DMS Error " + ' url: ' + url + ' code: ' + str(r.status_code))
        r.raise_for_status()
    return r.json()


def deleteObject(url, token):
    """ Responsible to check if authorization token is alive and retrieve information from the DMS"""

    if not isTokenValid(token.expirationDate):
        token = update_token(token)

    headers['authorization'] = token.token_type + ' ' + token.access_token
    r = requests.delete(url=url, headers=headers)
    if r.status_code != 204:
        logger.error("DMS Error " + ' url: ' + url + ' code: ' + str(r.status_code))
        r.raise_for_status()


def patchObject(url, token, body):
    """ Responsible to check if authorization token is alive and patch object to the DMS"""
    global r
    if not isTokenValid(token.expirationDate):
        token = update_token(token)

    headers['authorization'] = token.token_type + ' ' + token.access_token
    headers['Content-Type'] = "application/json"
    r = requests.patch(url=url, headers=headers, json=body)
    if r.status_code not in [200, 201]:
        logger.error("DMS Error " + ' url: ' + url + ' code: ' + str(r.status_code))
        r.raise_for_status()
    return r.json()


def isTokenValid(expirationDate):
    """ Compare expiration and current datetime
    and returns True if current datetime is prior to expiration datetime"""

    return False if str(datetime.utcnow()) >= expirationDate else True


def createUrl():
    url = urlDict['url'] + urlDict['uuid'] + urlDict['op']
    return url.strip()


def createUrlWithCriteria(request, url):
    for i in params:
        param = request.GET.get(str(i), None)
        paramName = str(i)
        url = addParamToUrl(param, paramName, url)
    return url[:-1] if url.endswith('&') else url


def addParamToUrl(param, paramName, url):
    if param is not None:
        param = param.strip()
        param = "?" + paramName + '=' + param if url.endswith('/') else paramName + '=' + param
        url = url + param if url.endswith('/') else url + '&' + param
    return url
