FROM python:3.8.12-slim-bullseye

ENV PYTHONUNBUFFERED=1
WORKDIR /app

RUN apt-get update && \
   apt-get install -y --no-install-recommends \
       uwsgi libmagic-dev git g++ liblapack-dev libatlas-base-dev && \
   rm -rf /var/lib/apt/lists/*

EXPOSE 8000

COPY requirements.txt manage.py authentication.py mydatabase /app/
RUN pip install --upgrade pip && \
    pip install -r requirements.txt

COPY uwsgi.ini /app/
COPY ./ddhlApi /app/ddhlApi
COPY ./models /app/models
COPY ./rabbitMQ /app/rabbitMQ

CMD ["uwsgi", "--ini", "/app/uwsgi.ini"]
